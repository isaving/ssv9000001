////Version: v0.0.1
package routers

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/isaving/sv/ssv9000001/controllers"
	"git.forms.io/universe/common/event_handler/register"
	"github.com/astaxie/beego"
)

// @Desc Select the appropriate transaction route according to TOPIC
// @Author
// @Date 2020-12-04
func InitRouter() error {

	eventRouteReg := register.NewEventHandlerRegister()

	bc := beego.AppConfig

	eventRouteReg.Router(bc.DefaultString(constant.TopicPrefix + "ssv9000001","SV900001"),&controllers.Ssv9000001Controller{}, "Ssv9000001")

	//eventRouteReg.Router("DASV0001",&controllers.Ssv9000001Controller{}, "DASV0001")

	return nil
}

// @Desc transaction router for swagger
// @Author
// @Date 2020-12-04
func init() {

	ns := beego.NewNamespace("isaving/v1/ssv9000001",
		beego.NSNamespace("/ssv9000001",
			beego.NSInclude(
				&controllers.Ssv9000001Controller{},
			),
		),
	)
	beego.AddNamespace(ns)
}
