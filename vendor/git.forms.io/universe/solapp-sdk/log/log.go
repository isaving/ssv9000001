//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package log

// func init() {
// 	fmt.Println("load log config")
// 	defer seelog.Flush()
// 	//加载配置文件
// 	logger, err := seelog.LoggerFromConfigAsFile("log.xml")
// 	if err != nil {
// 		panic("parse config.xml error")
// 	}
// 	//替换记录器
// 	seelog.ReplaceLogger(logger)
// }
import (
	"encoding/base64"
	"errors"
	"fmt"
	"net"
	"net/http"
	"os"
	"reflect"
	"strings"
	"sync"
	"time"

	"github.com/astaxie/beego"
	"github.com/sirupsen/logrus"
	"github.com/snowzach/rotatefilehook"

	"git.forms.io/universe/comm-agent/client"
	"git.forms.io/universe/solapp-sdk/util"
)

const (
	timeFormat = "20060102150405"
)

var logLevelMap = map[string]logrus.Level{
	"debug":   logrus.DebugLevel,
	"info":    logrus.InfoLevel,
	"warning": logrus.WarnLevel,
	"error":   logrus.ErrorLevel,
	"fatal":   logrus.FatalLevel,
	"panic":   logrus.PanicLevel,
}

func init() {
	bufferSize := beego.AppConfig.DefaultInt64("log::bufferSize", 1024000)
	logWriterNum := beego.AppConfig.DefaultInt("log::logWriterNum", 1)
	once.Do(func() {
		logChan = make(chan string, bufferSize)
		//	logrus.SetReportCaller(true)
		for i := 0; i < logWriterNum; i++ {
			go logWriter(logChan)
		}
	})
}

func setLogLevel() {
	logLevelConf := beego.AppConfig.String("log::logLevel")
	logLevel, ok := logLevelMap[logLevelConf]
	if ok {
		logrus.SetLevel(logLevel)
	}
}

func setLogRotateHook(logFile string) {
	maxSize, err := beego.AppConfig.Int("log.maxSize")
	if err != nil {
		maxSize = 200
	}

	maxAge, err := beego.AppConfig.Int("log.maxDays")
	if err != nil {
		maxAge = 7
	}

	logLevelConf := beego.AppConfig.String("log::logLevel")
	logLevel, ok := logLevelMap[logLevelConf]
	if !ok {
		logLevel = logrus.DebugLevel
	}

	customFormatter := logrus.JSONFormatter{}
	customFormatter.TimestampFormat = timeFormat

	rotateFileHook, err := rotatefilehook.NewRotateFileHook(rotatefilehook.RotateFileConfig{
		Filename:   logFile,
		MaxSize:    maxSize,
		MaxBackups: maxAge,
		MaxAge:     maxAge,
		Level:      logLevel,
		Formatter:  &customFormatter,
	})

	logrus.AddHook(rotateFileHook)
}

var logChan chan string
var once sync.Once

func logWriter(logChan chan string) {
	defer func() {
		logrus.Fatal("logWriter exit!!!")
	}()
	for {
		select {
		case log := <-logChan:
			{
				logrus.Infof(log)
			}
		}
	}
}

func InitLog() error {
	instanceID := os.Getenv("INSTANCE_ID")
	if "" == instanceID {
		logrus.Errorf("Failed to get env:%s", "INSTANCE_ID")
		return errors.New("Failed to get env:INSTANCE_ID")
	}
	//logPath := "/data/logs/" + instanceID
	preLogFileRootPath := beego.AppConfig.DefaultString("log::logFileRootPath", "/data/logs/")
	var logFileRootPath string
	if strings.HasSuffix(preLogFileRootPath, "/") {
		logFileRootPath = preLogFileRootPath
	} else {
		logFileRootPath = preLogFileRootPath + "/"
	}
	logPath := logFileRootPath + instanceID
	os.MkdirAll(logPath, os.ModePerm)
	var logFile string
	if beego.AppConfig.String("log::logFile") == "" {
		logFile = logPath + "/default.log"
	} else {
		logFile = logPath + "/" + beego.AppConfig.String("log::logFile")
	}
	logrus.Debugf("logpathfile==%s", logFile)
	f, err := os.OpenFile(logFile, os.O_WRONLY|os.O_CREATE, 0755)
	if err != nil {
		logrus.Errorf("Failed to open log file %s", logFile)
		return err
	}
	logrus.SetOutput(f)
	// 添加将日志打印到console的处理
	logrus.SetOutput(os.Stdout)
	setLogLevel()
	startLogLevelServer()
	setLogRotateHook(logFile)
	//bufferSize := beego.AppConfig.DefaultInt64("log::bufferSize", 1024000)
	//logWriterNum := beego.AppConfig.DefaultInt("log::logWriterNum", 1)
	//once.Do(func() {
	//	logChan = make(chan string, bufferSize)
	//	//	logrus.SetReportCaller(true)
	//	for i := 0; i < logWriterNum; i++ {
	//		go logWriter(logChan)
	//	}
	//})

	return nil
}

func getLogFields_V2(QJserialNo, serialNo, serviceID, funcName string, cost time.Duration, retCode string) logrus.Fields {
	fields := logrus.Fields{}
	fields["QJserialNo"] = QJserialNo
	fields["serialNo"] = serialNo
	fields["serviceID"] = serviceID
	fields["funcName"] = funcName
	fields["cost"] = cost
	fields["retCode"] = retCode
	return fields
}

func getLogFields() logrus.Fields {
	fields := logrus.Fields{}
	return fields
}

func Infof(format string, args ...interface{}) {
	if logrus.GetLevel() >= logrus.InfoLevel {
		logChan <- fmt.Sprintf(format, args...)
	}
}

func Info(args ...interface{}) {
	if logrus.GetLevel() >= logrus.InfoLevel {
		logChan <- fmt.Sprint(args...)
	}
}

func Errorf(format string, args ...interface{}) {
	if logrus.GetLevel() >= logrus.ErrorLevel {
		fields := getLogFields()
		logrus.WithFields(fields).Errorf(format, args...)
	}
}

func Error(args ...interface{}) {
	if logrus.GetLevel() >= logrus.ErrorLevel {
		fields := getLogFields()
		logrus.WithFields(fields).Error(args...)
	}
}

func Debugf(format string, args ...interface{}) {
	if logrus.GetLevel() >= logrus.DebugLevel {
		fields := getLogFields()
		logrus.WithFields(fields).Debugf(format, args...)
	}
}

func Debug(args ...interface{}) {
	if logrus.GetLevel() >= logrus.DebugLevel {
		fields := getLogFields()
		logrus.WithFields(fields).Debug(args...)
	}
}

//print msg log:added by leeingli on 20190918 for KTB
func Debugf_V4(format string, msg *client.UserMessage, args ...interface{}) {
	if logrus.GetLevel() >= logrus.DebugLevel {
		fields := getLogFields()
		a := util.MaskMsg(msg)
		newArgs := append([]interface{}{a}, args...)
		logrus.WithFields(fields).Debugf(format, newArgs...)
	}
}
func Debug_V4(msg *client.UserMessage, args ...interface{}) {
	if logrus.GetLevel() >= logrus.DebugLevel {
		fields := getLogFields()
		a := util.MaskMsg(msg)
		newArgs := append([]interface{}{a}, args...)
		logrus.WithFields(fields).Debug(newArgs...)
	}
}
func Infof_V4(format string, msg *client.UserMessage, args ...interface{}) {
	if logrus.GetLevel() >= logrus.InfoLevel {
		fields := getLogFields()
		a := util.MaskMsg(msg)
		newArgs := append([]interface{}{a}, args...)
		logrus.WithFields(fields).Infof(format, newArgs...)
	}
}
func Info_V4(msg *client.UserMessage, args ...interface{}) {
	if logrus.GetLevel() >= logrus.InfoLevel {
		fields := getLogFields()
		a := util.MaskMsg(msg)
		newArgs := append([]interface{}{a}, args...)
		logrus.WithFields(fields).Info(newArgs...)
	}
}
func Errorf_V4(format string, msg *client.UserMessage, args ...interface{}) {
	if logrus.GetLevel() >= logrus.ErrorLevel {
		fields := getLogFields()
		a := util.MaskMsg(msg)
		newArgs := append([]interface{}{a}, args...)
		logrus.WithFields(fields).Errorf(format, newArgs...)
	}
}
func Error_V4(msg *client.UserMessage, args ...interface{}) {
	if logrus.GetLevel() >= logrus.ErrorLevel {
		fields := getLogFields()
		a := util.MaskMsg(msg)
		newArgs := append([]interface{}{a}, args...)
		logrus.WithFields(fields).Error(newArgs...)
	}
}

//print msg log:added by leeingli on 20190918 for KTB

func IfError(err error, action string) bool {
	if err == nil || reflect.ValueOf(err).IsNil() {
		return false
	} else {
		Errorf("%s fail,as:%s", action, err.Error())
		return true
	}

}

func IfErrorf(err error, format string, args ...interface{}) bool {
	if err == nil || reflect.ValueOf(err).IsNil() {
		return false
	} else {
		action := fmt.Sprintf(format, args...)
		Errorf("%s fail,as:%s", action, err.Error())
		return true
	}

}

func Info_V2(QJserialNo, serialNo, serviceID, funcName string, cost time.Duration, retCode string) {
	if logrus.GetLevel() >= logrus.InfoLevel {
		fields := getLogFields_V2(QJserialNo, serialNo, serviceID, funcName, cost, retCode)
		logrus.WithFields(fields).Info()
	}
}

func Error_V2(QJserialNo, serialNo, serviceID, funcName string, cost time.Duration, retCode string) {
	if logrus.GetLevel() >= logrus.ErrorLevel {
		fields := getLogFields_V2(QJserialNo, serialNo, serviceID, funcName, cost, retCode)
		logrus.WithFields(fields).Error()
	}
}

func Debug_V2(QJserialNo, serialNo, serviceID, funcName string, cost time.Duration, retCode string) {
	if logrus.GetLevel() >= logrus.DebugLevel {
		fields := getLogFields_V2(QJserialNo, serialNo, serviceID, funcName, cost, retCode)
		logrus.WithFields(fields).Debug()
	}
}

//added by nanili on 20190823 for print audit log
func getAuditLogFields(user, operator, operatorLevel string, request, response []byte, matedata ...Option) logrus.Fields {
	var option = &options{make(map[string]string)}
	for _, f := range matedata {
		f(option)
	}
	fields := logrus.Fields{}
	fields["user"] = user
	fields["operator"] = operator
	fields["request"] = string(request)
	fields["response"] = string(response)
	fields["operatorLevel"] = operatorLevel
	fields["logtype"] = "audit"
	for k, v := range option.m {
		fields[k] = v
	}
	//每条增加日志类型；或者不同类型的日志输出到不同的日志文件。
	//fields["Model"] = model
	//rapm采集日志之后自动拼接上service和instance

	return fields
}

type options struct {
	m map[string]string
}
type Option func(o *options)

func MateDataWithMap(m map[string]string) Option {
	return func(o *options) {
		o.m = m
	}
}
func MateData(key, value string) Option {
	return func(o *options) {
		o.m[key] = value
	}
}

func Debug_Audit(user, operator, operatorLevel string, request, response []byte, matedata ...Option) {
	if logrus.GetLevel() >= logrus.DebugLevel {
		fields := getAuditLogFields(user, operator, operatorLevel, request, response, matedata...)
		logrus.WithFields(fields).Debug()
	}
}
func Info_Audit(user, operator, operatorLevel string, request, response []byte, matedata ...Option) {
	if logrus.GetLevel() >= logrus.InfoLevel {
		fields := getAuditLogFields(user, operator, operatorLevel, request, response, matedata...)
		logrus.WithFields(fields).Info()
	}
}
func Error_Audit(user, operator, operatorLevel string, request, response []byte, matedata ...Option) {
	if logrus.GetLevel() >= logrus.ErrorLevel {
		fields := getAuditLogFields(user, operator, operatorLevel, request, response, matedata...)
		logrus.WithFields(fields).Error()
	}
}

//added by nanili on 20190813 for padss PAN
func getLogFields_V3(pan string) logrus.Fields {
	fields := logrus.Fields{}
	fields["pan"] = base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%s", pan)))
	return fields
}

func Debug_V3(pan string, args ...interface{}) {
	if logrus.GetLevel() >= logrus.DebugLevel {
		fields := getLogFields_V3(pan)
		logrus.WithFields(fields).Debug(args...)
	}
}
func Debugf_V3(pan, format string, args ...interface{}) {
	if logrus.GetLevel() >= logrus.DebugLevel {
		fields := getLogFields_V3(pan)
		logrus.WithFields(fields).Debugf(format, args...)
	}
}
func Error_V3(pan string, args ...interface{}) {
	if logrus.GetLevel() >= logrus.ErrorLevel {
		fields := getLogFields_V3(pan)
		logrus.WithFields(fields).Error(args...)
	}
}
func Errorf_V3(pan, format string, args ...interface{}) {
	if logrus.GetLevel() >= logrus.ErrorLevel {
		fields := getLogFields_V3(pan)
		logrus.WithFields(fields).Errorf(format, args...)
	}
}
func Info_V3(pan string, args ...interface{}) {
	if logrus.GetLevel() >= logrus.InfoLevel {
		fields := getLogFields_V3(pan)
		logrus.WithFields(fields).Info(args...)
	}
}
func Infof_V3(pan, format string, args ...interface{}) {
	if logrus.GetLevel() >= logrus.InfoLevel {
		fields := getLogFields_V3(pan)
		logrus.WithFields(fields).Infof(format, args...)
	}
}

//---------------------------下面的代码用于动态设定日志等级--------------------------------//

// Server structure is used to the store backend information
type Server struct {
	SocketLocation string
	Debug          bool
}

// StartServerWithDefaults starts the server with default values
func startLogLevelServer() {
	s := Server{
		SocketLocation: beego.AppConfig.String("log::logLevelUnixSocket"),
	}
	s.Start()
}

// Start the server
func (s *Server) Start() {
	os.Remove(s.SocketLocation)
	go s.ListenAndServe()
}

// ListenAndServe is used to setup handlers and
// start listening on the specified location
func (s *Server) ListenAndServe() error {
	logrus.Infof("Listening on %s", s.SocketLocation)
	server := http.Server{}
	http.HandleFunc("/v1/loglevel", s.loglevel)
	socketListener, err := net.Listen("unix", s.SocketLocation)
	if err != nil {
		return err
	}
	return server.Serve(socketListener)
}

func (s *Server) loglevel(rw http.ResponseWriter, req *http.Request) {
	// curl -X POST -d "level=debug" localhost:12345/v1/loglevel
	logrus.Debugf("Received loglevel request")
	if req.Method == http.MethodGet {
		level := logrus.GetLevel().String()
		rw.Write([]byte(fmt.Sprintf("%s\n", level)))
	}

	if req.Method == http.MethodPost {
		if err := req.ParseForm(); err != nil {
			rw.WriteHeader(http.StatusInternalServerError)
			rw.Write([]byte(fmt.Sprintf("Failed to parse form: %v\n", err)))
		}
		level, err := logrus.ParseLevel(req.Form.Get("level"))
		if err != nil {
			rw.WriteHeader(http.StatusInternalServerError)
			rw.Write([]byte(fmt.Sprintf("Failed to parse loglevel: %v\n", err)))
		} else {
			logrus.SetLevel(level)
			rw.Write([]byte("OK\n"))
		}
	}
}
