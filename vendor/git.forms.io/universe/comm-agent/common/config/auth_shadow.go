package config

import (
	"encoding/json"
	log "git.forms.io/universe/comm-agent/common/log"
	"github.com/go-errors/errors"
	"io/ioutil"
	"os"
)

var AuthShadowConfigs []AuthShadowConfig

var authShadowFilePath = "./conf/shadow.json"

type AuthShadowConfig struct {
	Name     string            `json:"name"`
	CodeBook map[string]string `json:"codeBook"`
}

func FileExists(name string) bool {
	if _, err := os.Stat(name); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}

func InitAuthShadownConfig() error {
	AuthShadowConfigs = []AuthShadowConfig{}

	if !FileExists(authShadowFilePath) {
		log.Infof("file [%s] not exists, skip loading...", authShadowFilePath)
		return nil
	}

	jsonFile, err := os.Open(authShadowFilePath)
	defer jsonFile.Close()

	if nil != err {
		return errors.Wrap(err, 0)
	}

	var bs []byte
	bs, err = ioutil.ReadAll(jsonFile)
	if err != nil {
		return errors.Wrap(err, 0)
	}

	if err = json.Unmarshal(bs, &AuthShadowConfigs); err != nil {
		return errors.Wrap(err, 0)
	}

	return nil
}
