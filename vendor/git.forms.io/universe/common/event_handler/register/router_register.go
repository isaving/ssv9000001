//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package register

import (
	log "git.forms.io/universe/comm-agent/common/log"
	"git.forms.io/universe/common/event_handler/base"
	"git.forms.io/universe/common/event_handler/constant"
	"github.com/go-errors/errors"
	"reflect"
	"regexp"
	"strings"
	"sync"
)

var (
	Router *EventRouterRegister
	once   sync.Once
	lock   sync.RWMutex
)

type EventRouterRegister struct {
	DefiniteEventHandlers   map[string]*HandlerProperty
	PrefixEventHandlers     map[string]*HandlerProperty
	SuffixEventHandlers     map[string]*HandlerProperty
	ExpressionEventHandlers map[string]*HandlerProperty
}

type EventRouter interface {
	Router(topicName string, controller interface{}, method string)
}

type HandlerProperty struct {
	HandlerType reflect.Type
	MethodName  string
}

func NewEventHandlerRegister() *EventRouterRegister {
	once.Do(func() {
		Router = &EventRouterRegister{
			DefiniteEventHandlers:   make(map[string]*HandlerProperty),
			PrefixEventHandlers:     make(map[string]*HandlerProperty),
			SuffixEventHandlers:     make(map[string]*HandlerProperty),
			ExpressionEventHandlers: make(map[string]*HandlerProperty),
		}
	})

	return Router
}

func (e EventRouterRegister) genHandlerProperty(topicExp string, handler base.EventHandlerInterface, method string, regType string) *HandlerProperty {
	handlerType := reflect.TypeOf(handler)

	if _, ok := handlerType.MethodByName(constant.CONTROLLER_NAME_GET_METHOD); ok {
		handlerIns := reflect.ValueOf(handler)
		targetMethod := handlerIns.MethodByName(constant.CONTROLLER_NAME_GET_METHOD)
		targetMethodResult := targetMethod.Call(nil)
		if "" == topicExp {
			//panic(fmt.Sprintf("Topic ID is empty,please check,Handler[%v] - Method[%s]", targetMethodResult[0].String(), method))
			panic(errors.Errorf("%s is empty,please check,Handler[%v] - Method[%s]", regType, targetMethodResult[0].String(), method))
		}
		log.Infof("Event router register: %s[%s] - Handler[%v] - Method[%s]", regType, topicExp, targetMethodResult[0].String(), method)
	} else {
		if "" == topicExp {
			//panic(fmt.Sprintf("Topic ID is empty,please check,Handler[%v] - Method[%s]", handler, method))
			panic(errors.Errorf("%s is empty,please check,Handler[%v] - Method[%s]", regType, handler, method))
		}
		log.Infof("Event router register: %s[%s] - Handler[%p] - Method[%s]", regType, topicExp, handler, method)
	}

	return &HandlerProperty{reflect.TypeOf(handler).Elem(), method}
}

func (e EventRouterRegister) Router(topicId string, handler base.EventHandlerInterface, method string) {
	lock.Lock()
	defer lock.Unlock()

	handlerProperty := e.genHandlerProperty(topicId, handler, method, "Topic ID")

	e.DefiniteEventHandlers[topicId] = handlerProperty
}

func (e EventRouterRegister) RouterPrefix(prefixOfTopicId string, handler base.EventHandlerInterface, method string) {
	lock.Lock()
	defer lock.Unlock()

	handlerProperty := e.genHandlerProperty(prefixOfTopicId, handler, method, "Prefix of Topic ID")

	e.PrefixEventHandlers[prefixOfTopicId] = handlerProperty
}

func (e EventRouterRegister) RouterSuffix(suffixOfTopicId string, handler base.EventHandlerInterface, method string) {
	lock.Lock()
	defer lock.Unlock()

	handlerProperty := e.genHandlerProperty(suffixOfTopicId, handler, method, "Suffix of Topic ID")

	e.SuffixEventHandlers[suffixOfTopicId] = handlerProperty
}

func (e EventRouterRegister) RouterExp(topicIdExp string, handler base.EventHandlerInterface, method string) {
	lock.Lock()
	defer lock.Unlock()

	handlerProperty := e.genHandlerProperty(topicIdExp, handler, method, "Topic ID Expression")

	e.ExpressionEventHandlers[topicIdExp] = handlerProperty
}

func (e EventRouterRegister) MatchHandler(topicId string) (*HandlerProperty, error) {
	lock.RLock()
	defer lock.RUnlock()

	// 1. DefiniteEventHandlers
	if hp, ok := e.DefiniteEventHandlers[topicId]; ok {
		return hp, nil
	}

	// 2. PrefixEventHandlers
	for k, v := range e.PrefixEventHandlers {
		if strings.HasPrefix(topicId, k) {
			return v, nil
		}
	}

	// 3. SuffixEventHandlers
	for k, v := range e.SuffixEventHandlers {
		if strings.HasSuffix(topicId, k) {
			return v, nil
		}
	}

	// 4. ExpressionEventHandlers
	for k, v := range e.ExpressionEventHandlers {
		if isOk, error := regexp.MatchString(k, topicId); isOk {
			return v, nil
		} else if nil != error {
			return nil, errors.Wrap(error, 1)
		}
	}

	return nil, nil
}
