//Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.

package repository

import (
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/common/log"
	"git.forms.io/universe/dts/common/util"
	"github.com/pkg/errors"
	"sync"
)

var (
	//CmpTxnRep CompensableTxnRepository = &MemoryRepository{storage: make(map[string]*client.TransactionInvocation)}
	CmpTxnRep CompensableTxnRepository = NewMemoryRepository()
)

const SHARD_COUNT = 32

// @Desc a struct to keep TransactionInvocation by Memory
type ConcurrentMapShared struct {
	items map[string]*client.TransactionInvocation
	index int
	sync.RWMutex
}

type ConcurrentMap []*ConcurrentMapShared

// @Desc get ConcurrentMapShared
func (m ConcurrentMap) GetShard(key string) *ConcurrentMapShared {
	return m[uint(util.Fnv32(key))%uint(SHARD_COUNT)]
}

// @Desc a interface to operate transaction parameters
type CompensableTxnRepository interface {
	Save(txnInvocation *client.TransactionInvocation) (int, error)
	FindTxnByXid(rootXid, branchXid string) *client.TransactionInvocation
	DeleteTxnByXid(rootXid, branchXid string) error
}

// @Desc get CompensableTxnRepository, which is a MemoryRepository actually
func NewMemoryRepository() CompensableTxnRepository {
	compensableTxnRepository := &MemoryRepository{}

	compensableTxnRepository.shardStorage = make(ConcurrentMap, SHARD_COUNT)

	for i := 0; i < SHARD_COUNT; i++ {
		compensableTxnRepository.shardStorage[i] = &ConcurrentMapShared{
			items: make(map[string]*client.TransactionInvocation),
			index: i,
		}
	}

	return compensableTxnRepository
}

// @Desc a struct whicn use ConcurrentMapShared as attribute
type MemoryRepository struct {
	//storage map[string]*client.TransactionInvocation
	//sync.RWMutex

	shardStorage ConcurrentMap
}

// @Desc implements CompensableTxnRepository's Save method to save transaction's Invocation
// @Param txnInvocation transaction's Invocation
// @Return result the number of saved transaction
// @Return err error
func (m *MemoryRepository) Save(txnInvocation *client.TransactionInvocation) (result int, err error) {
	// shard map
	sm := m.shardStorage.GetShard(txnInvocation.RootXid)
	log.Debugf("[Save]Shard key:[%s], Shard index:[%d]", txnInvocation.RootXid, sm.index)

	sm.Lock()
	defer sm.Unlock()

	if _, ok := sm.items[txnInvocation.BranchXid]; ok {
		err = errors.Errorf("Save failed, Transaction invocation is already exists!")
		return
	}

	sm.items[txnInvocation.BranchXid] = txnInvocation

	//m.Lock()
	//defer m.Unlock()
	//
	//if _, ok := m.storage[txnInvocation.BranchXid]; ok {
	//	err = errors.Errorf("Save failed, Transaction invocation is already exists!")
	//	return
	//}
	//
	//m.storage[txnInvocation.BranchXid] = txnInvocation

	return
}

// @Desc implements CompensableTxnRepository's FindTxnByXid method to find transaction's Invocation by rootXid and branchXid
// @Param rootXid transaction's rootXid
// @Param branchXid transaction's branchXid
// @Return result the transaction's Invocation
func (m *MemoryRepository) FindTxnByXid(rootXid, branchXid string) (result *client.TransactionInvocation) {
	// shard map
	sm := m.shardStorage.GetShard(rootXid)
	log.Debugf("[FindTxnByXid]Shard key:[%s], Shard index:[%d]", rootXid, sm.index)

	sm.RLock()
	defer sm.RUnlock()

	if txnInvocation, ok := sm.items[branchXid]; !ok {
		return
	} else {
		result = txnInvocation
	}

	//m.RLock()
	//defer m.RUnlock()
	//
	//if txnInvocation, ok := m.storage[xid]; !ok {
	//	return
	//} else {
	//	result = txnInvocation
	//}

	return
}

// @Desc implements CompensableTxnRepository's DeleteTxnByXid method to delete transaction's Invocation by rootXid and branchXid
// @Param rootXid transaction's rootXid
// @Param branchXid transaction's branchXid
// @Return err error
func (m *MemoryRepository) DeleteTxnByXid(rootXid, branchXid string) (err error) {
	// shard map
	sm := m.shardStorage.GetShard(rootXid)
	log.Debugf("[DeleteTxnByXid]Shard key:[%s], Shard index:[%d]", rootXid, sm.index)

	sm.Lock()
	defer sm.Unlock()

	delete(sm.items, branchXid)

	//m.Lock()
	//defer m.Unlock()
	//
	//delete(m.storage, xid)

	return
}
