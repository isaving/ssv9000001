//Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.

package util

import (
	"fmt"
	"github.com/astaxie/beego"
	"net"
	"reflect"
	"strconv"
	"sync"
	"time"
)

func IndexOfItem(value interface{}, array interface{}) int {
	switch reflect.TypeOf(array).Kind() {
	case reflect.Slice:
		s := reflect.ValueOf(array)
		for i := 0; i < s.Len(); i++ {
			if reflect.DeepEqual(value, s.Index(i).Interface()) {
				return i
			}
		}
	}

	return -1
}

func CurrentTime() string {
	t := time.Now()
	return fmt.Sprintf("%04d-%02d-%02d %02d:%02d:%02d.%09d",
		//t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second(), strconv.Itoa(t.Nanosecond())[0:3])
		t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second(), t.Nanosecond())
}

func FormatTime(t time.Time) string {
	return fmt.Sprintf("%04d-%02d-%02d %02d:%02d:%02d.%09d",
		//t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second(), strconv.Itoa(t.Nanosecond())[0:3])
		t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second(), t.Nanosecond())
}

func ToTime(s string) time.Time {
	timeLayout := "2006-01-02 15:04:05"
	loc, _ := time.LoadLocation("Local")
	theTime, _ := time.ParseInLocation(timeLayout, s, loc)
	return theTime
}

func CurrentHost() (host string) {
	host = "localhost"
	netInterfaces, e := net.Interfaces()
	if e != nil {
		return
	}

	for i := 0; i < len(netInterfaces); i++ {
		if (netInterfaces[i].Flags & net.FlagUp) != 0 {
			addrs, _ := netInterfaces[i].Addrs()

			for _, address := range addrs {
				if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() && ipnet.IP.To4() != nil {
					host = ipnet.IP.String()
					return
				}
			}
		}
	}

	return
}

func Fnv32(key string) uint32 {
	hash := uint32(2166136261)
	const prime32 = uint32(16777619)
	for i := 0; i < len(key); i++ {
		hash *= prime32
		hash ^= uint32(key[i])
	}
	return hash
}

var flagNo int64 = 0
var lock sync.Mutex

const SEQMAX = 999999

var dcnNo = beego.AppConfig.DefaultString("service::dataCenterNode", "000000")

func GenerateSerialNo(instanceID string, tp string) string {
	timestamp := time.Now().Unix()

	serialNo := tp + dcnNo + instanceID + strconv.FormatInt(timestamp, 10)

	lock.Lock()
	defer lock.Unlock()
	flagNo++
	if flagNo > SEQMAX {
		flagNo = 1
	}
	flagNo2Str := strconv.FormatInt(flagNo, 10)
	flagNo2Str = fmt.Sprintf("%06v", flagNo2Str)
	serialNo = serialNo + flagNo2Str
	return serialNo
}
