//Version: v0.0.1
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SSV9000001I struct {
	AgreementId		string `validate:"required",json:"AgreementId"`		//合约号
	PrductId		string `validate:"required",json:"PrductId"`		//产品号
	PrductNm        int    `validate:"required",json:"PrductNm"`        //产品顺序号
	AgreementType   string `validate:"required",json:"AgreementType"`   //合约类型
	Currency        string `validate:"required",json:"Currency"`        //币种
	PeriodFlag      string `validate:"required",json:"PeriodFlag"`      //是否分期标志
	CashTranFlag    string `validate:"required",json:"CashTranFlag"`    //钞汇标识
	CstmrId         string `validate:"required",json:"CstmrId"`         //客户编号
	CstmrTyp        string `validate:"required",json:"CstmrTyp"`        //客户类型
	AccuntNme       string `validate:"required",json:"AccuntNme"`       //账户名称
	UsgCod          string `validate:"required",json:"UsgCod"`          //用途代码
	WdrwlMthd       string `validate:"required",json:"WdrwlMthd"`       //支取方式
	AccPsw          string `validate:"required",json:"AccPsw"`          //账户密码
	TrnWdrwMthd     string `validate:"required",json:"TrnWdrwMthd"`     //通兑方式
	DepCreFlag      string `validate:"required",json:"DepCreFlag"`      //借贷记标志
	AccFlag         string `validate:"required",json:"AccFlag"`         //是否核算标志
	Account         string `validate:"required",json:"Account"`         //核算账号
	TermFlag		string `validate:"required",json:""`                //是否有存期信息
	BegnTxDt        string `validate:"required",json:"BegnTxDt"`        //起息日期
	AppntDtFlg      string `validate:"required",json:"AppntDtFlg"`      //是否指定到期日
	AppntDt         string `validate:"required",json:"AppntDt"`         //到期日期
	OpnAmt          float64 `validate:"required",json:"OpnAmt"`          //开户金额
	AccCnclFlg      string `validate:"required",json:"AccCnclFlg"`      //销户启用标识
	AutCnl          string `validate:"required",json:"AutCnl"`          //是否允许自动销户
	DytoCncl        int   `validate:"required",json:"DytoCncl"`        //自动销户宽限天数
	SttmntFlg       string `validate:"required",json:"SttmntFlg"`       //对账单标识
	WthdrwlMthd     string `validate:"required",json:"WthdrwlMthd"`     //取款方式
	OvrDrwFlg       string `validate:"required",json:"OvrDrwFlg"`       //透支标识
	DpTerm          int    `validate:"required",json:"DpTerm"`          //存期
	DpTermUn        string `validate:"required",json:"DpTermUn"`        //存期单位
	HolidayFlag     string `validate:"required",json:"HolidayFlag"`     //节假日处理标志
	AdvnWithdTm     int    `validate:"required",json:"AdvnWithdTm"`     //允许提前支取次数
	MatryWithdTm    int    `validate:"required",json:"MatryWithdTm"`    //允许到期支取次数
	VerDueWithdTm   int    `validate:"required",json:"VerDueWithdTm"`   //允许逾期支取次数
	LwWithAmt       float64 `validate:"required",json:"LwWithAmt"`       //最低支取金额
	LwKepAmt        float64 `validate:"required",json:"LwKepAmt"`        //最低留存金额
	AprvWithDt      string `validate:"required",json:"AprvWithDt"`      //允许支取日期
	GrcWithDy       int    `validate:"required",json:"GrcWithDy"`       //提前支取宽限天数
	AllwDpsit       int    `validate:"required",json:"AllwDpsit"`       //是否允许再次存款
	DuePrcesFlag    string `validate:"required",json:"DuePrcesFlag"`    //到期处理标识
	PrncplPrcesFlag string `validate:"required",json:"PrncplPrcesFlag"` //本金处理方式
	MinRenAmt       float64 `validate:"required",json:"MinRenAmt"`       //最低续存金额
	RenNm           string `validate:"required",json:"RenNm"`           //续存总期数
	PrncplRenFlag   string `validate:"required",json:"PrncplRenFlag"`   //本金续存到期处理方式
	TexRenFlag      string `validate:"required",json:"TexRenFlag"`      //利息续存到期处理方式
	CstmrCntctAdd   string `validate:"required",json:"CstmrCntctAdd"`   //客户联系地址
	CstmrCntctPh    string `validate:"required",json:"CstmrCntctPh"`    //客户联系电话
	CstmrCntctEm    string `validate:"required",json:"CstmrCntctPh"`    //邮箱地址
	BnCrtAcc        string `validate:"required",json:"BnCrtAcc"`        //开户行所
	BnAcc           string `validate:"required",json:"BnACC"`           //账务行所
	AccOpnDt        string `validate:"required",json:"AccOpnDt"`        //开户日期
	AccOpnEm        string `validate:"required",json:"AccOpnEm"`        //开户柜员
	RequrstId       string `validate:"required",json:"RequrstID"`       //外围流水号
}

type SSV9000001O struct {
	AgreementID		string	//合约号

}

// @Desc Build request message
func (o *SSV9000001I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SSV9000001I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SSV9000001O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SSV9000001O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SSV9000001I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SSV9000001I) GetServiceKey() string {
	return "ssv9000001"
}
