package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACRTC0I struct {
	AcctgAcctNo		string ` validate:"required,max=40"`
	BalanceType		string
	PeriodNum		int64
}

type DAACRTC0O struct {
	RecordNo            int64
	AcctgAcctNo         string
	BalanceType         string
	PeriodNum           int64
	CurrIntStDate       string
	CurrIntEndDate      string
	UnpaidInt           float64
	RepaidInt           float64
	AccmWdAmt           float64
	AccmIntSetlAmt      float64
	UnpaidIntAmt        float64
	RepaidIntAmt        float64
	AlrdyTranOffshetInt float64
	DcValueInt          float64
	CavInt              float64
	OnshetInt           float64
	FrzAmt              float64
	Status              string
	LastCalcDate        string
	LastMaintDate       string
	LastMaintTime       string
	LastMaintBrno       string
	LastMaintTell       string
	AcruUnstlIntr       float64
	AccmCmpdAmt         float64

}

type DAACRTC0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACRTC0I
}

type DAACRTC0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACRTC0O
}

type DAACRTC0RequestForm struct {
	Form []DAACRTC0IDataForm
}

type DAACRTC0ResponseForm struct {
	Form []DAACRTC0ODataForm
}

// @Desc Build request message
func (o *DAACRTC0RequestForm) PackRequest(DAACRTC0I DAACRTC0I) (responseBody []byte, err error) {

	requestForm := DAACRTC0RequestForm{
		Form: []DAACRTC0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRTC0I",
				},
				FormData: DAACRTC0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACRTC0RequestForm) UnPackRequest(request []byte) (DAACRTC0I, error) {
	DAACRTC0I := DAACRTC0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACRTC0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRTC0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACRTC0ResponseForm) PackResponse(DAACRTC0O DAACRTC0O) (responseBody []byte, err error) {
	responseForm := DAACRTC0ResponseForm{
		Form: []DAACRTC0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRTC0O",
				},
				FormData: DAACRTC0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACRTC0ResponseForm) UnPackResponse(request []byte) (DAACRTC0O, error) {

	DAACRTC0O := DAACRTC0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACRTC0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRTC0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACRTC0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
