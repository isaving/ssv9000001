package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type AC020004I struct {
	AcctgAcctNo	string `valid:"Required;MaxSize(20)"` //贷款核算账号
	Status		string `valid:"Required;MaxSize(10)"` // 余额状态
}

type AC020004RequestForm struct {
	Form []AC020004IDataForm `json:"Form"`
}

type AC020004IDataForm struct {
	FormHead CommonFormHead		`json:"FormHead"`
	FormData AC020004I		`json:"FormData"`
}



type AC020004O struct {
	AcctgAcctNo	string	`json:"AcctgAcctNo"`
	Status		string	`json:"status"`
}

type AC020004ResponseForm struct {
	Form []AC020004ODataForm `json:"Form"`
}

type AC020004ODataForm struct {
	FormHead CommonFormHead `json:"FormHead"`
	FormData AC020004O      `json:"FormData"`
}

// @Desc Parsing request message
func (w *AC020004RequestForm) UnPackRequest(req []byte) (AC020004I, error) {
	AC020004I := AC020004I{}
	if err := json.Unmarshal(req, w); nil != err {
		return AC020004I, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	if len(w.Form) < 1 {
		return AC020004I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return w.Form[0].FormData, nil
}

// @Desc Build response message
func (w *AC020004ResponseForm) PackResponse(AC020004OData AC020004O) (res []byte, err error) {
	resForm := AC020004ResponseForm{
		Form: []AC020004ODataForm{
			AC020004ODataForm{
				FormHead: CommonFormHead{
					FormId: "AC020004O001",
				},
				FormData: AC020004OData,
			},
		},
	}

	rspBody, err := json.Marshal(resForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return rspBody, nil
}

// @Desc Build request message
func (w *AC020004RequestForm) PackRequest(AC020004IData AC020004I) (res []byte, err error) {
	requestForm := AC020004RequestForm{
		Form: []AC020004IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC020004I001",
				},
				FormData: AC020004IData,
			},
		},
	}

	rspBody, err := json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return rspBody, nil
}

// @Desc Parsing response message
func (w *AC020004ResponseForm) UnPackResponse(req []byte) (AC020004O, error) {
	AC020004O := AC020004O{}

	if err := json.Unmarshal(req, w); nil != err {
		return AC020004O, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	if len(w.Form) < 1 {
		return AC020004O, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return w.Form[0].FormData, nil
}

func (w *AC020004I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}