package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACRBT0I struct {
	IntPlanNo	string ` validate:"required,max=20"`
	Lvls		int64
}

type DAACRBT0O struct {
	Back1                 interface{} `json:"Back1"`
	Back2                 interface{} `json:"Back2"`
	Back3                 interface{} `json:"Back3"`
	Back4                 interface{} `json:"Back4"`
	Back5                 interface{} `json:"Back5"`
	Dep                   int         `json:"Dep"`
	FixdIntRate           float64     `json:"FixdIntRate"`
	FixdIntRateEffectDate string      `json:"FixdIntRateEffectDate"`
	Flag1                 interface{} `json:"Flag1"`
	Flag2                 interface{} `json:"Flag2"`
	FloatFlag             string      `json:"FloatFlag"`
	FloatOption           string      `json:"FloatOption"`
	FloatRate             float64     `json:"FloatRate"`
	IntFlag               string      `json:"IntFlag"`
	IntPlanNo             string      `json:"IntPlanNo"`
	IntRateNo             string      `json:"IntRateNo"`
	IntRateUseFlag        string      `json:"IntRateUseFlag"`
	LastMaintBrno         interface{} `json:"LastMaintBrno"`
	LastMaintDate         interface{} `json:"LastMaintDate"`
	LastMaintTell         interface{} `json:"LastMaintTell"`
	LastMaintTime         interface{} `json:"LastMaintTime"`
	Lvls                  int         `json:"Lvls"`
	LvlsAmt               int         `json:"LvlsAmt"`
	MinIntRate            float64     `json:"MinIntRate"`
	TccState              interface{} `json:"TccState"`
}

type DAACRBT0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACRBT0I
}

type DAACRBT0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACRBT0O
}

type DAACRBT0RequestForm struct {
	Form []DAACRBT0IDataForm
}

type DAACRBT0ResponseForm struct {
	Form []DAACRBT0ODataForm
}

// @Desc Build request message
func (o *DAACRBT0RequestForm) PackRequest(DAACRBT0I DAACRBT0I) (responseBody []byte, err error) {

	requestForm := DAACRBT0RequestForm{
		Form: []DAACRBT0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRBT0I",
				},
				FormData: DAACRBT0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACRBT0RequestForm) UnPackRequest(request []byte) (DAACRBT0I, error) {
	DAACRBT0I := DAACRBT0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACRBT0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRBT0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACRBT0ResponseForm) PackResponse(DAACRBT0O DAACRBT0O) (responseBody []byte, err error) {
	responseForm := DAACRBT0ResponseForm{
		Form: []DAACRBT0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRBT0O",
				},
				FormData: DAACRBT0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACRBT0ResponseForm) UnPackResponse(request []byte) (DAACRBT0O, error) {

	DAACRBT0O := DAACRBT0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACRBT0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRBT0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACRBT0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
