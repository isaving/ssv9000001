package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACRIP0I struct {
	IntPlanNo	string ` validate:"required,max=20"`
}

type DAACRIP0O struct {
	AmtLvlsFlag         string      `json:"AmtLvlsFlag"`
	Back1               interface{} `json:"Back1"`
	Back2               interface{} `json:"Back2"`
	Back3               interface{} `json:"Back3"`
	BankNo              string      `json:"BankNo"`
	CornerFlag          string      `json:"CornerFlag"`
	CritocalPointFlag   string      `json:"CritocalPointFlag"`
	Currency            string      `json:"Currency"`
	DefFloatRate        float64     `json:"DefFloatRate"`
	FixdIntRate         float64     `json:"FixdIntRate"`
	Flag1               interface{} `json:"Flag1"`
	Flag2               interface{} `json:"Flag2"`
	FloatCeil           float64     `json:"FloatCeil"`
	FloatFlag           string      `json:"FloatFlag"`
	FloatFloor          float64     `json:"FloatFloor"`
	FloatOption         string      `json:"FloatOption"`
	IntCalcOption       string      `json:"IntCalcOption"`
	IntFlag             string      `json:"IntFlag"`
	IntPlanDesc         interface{} `json:"IntPlanDesc"`
	IntPlanNo           string      `json:"IntPlanNo"`
	IntRateNo           string      `json:"IntRateNo"`
	IntRateUseFlag      string      `json:"IntRateUseFlag"`
	LastMaintBrno       interface{} `json:"LastMaintBrno"`
	LastMaintDate       interface{} `json:"LastMaintDate"`
	LastMaintTell       interface{} `json:"LastMaintTell"`
	LastMaintTime       interface{} `json:"LastMaintTime"`
	LoanIntRateAdjCycle string      `json:"LoanIntRateAdjCycle"`
	LoanIntRateAdjFreq  string      `json:"LoanIntRateAdjFreq"`
	LoanIntRateAdjType  string      `json:"LoanIntRateAdjType"`
	LvlsIntRateAmtType  string      `json:"LvlsIntRateAmtType"`
	LyrdFlag            string      `json:"LyrdFlag"`
	LyrdTermUnit        string      `json:"LyrdTermUnit"`
	MinIntAmt           float64     `json:"MinIntAmt"`
	MinIntRate          float64     `json:"MinIntRate"`
	MonIntUnit          string      `json:"MonIntUnit"`
	TccState            interface{} `json:"TccState"`
	TermAmtPrtyFlag     string      `json:"TermAmtPrtyFlag"`
	TermLvlsFlag        string      `json:"TermLvlsFlag"`
	TolLvls             int         `json:"TolLvls"`
	YrIntUnit           string      `json:"YrIntUnit"`
}

type DAACRIP0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACRIP0I
}

type DAACRIP0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACRIP0O
}

type DAACRIP0RequestForm struct {
	Form []DAACRIP0IDataForm
}

type DAACRIP0ResponseForm struct {
	Form []DAACRIP0ODataForm
}

// @Desc Build request message
func (o *DAACRIP0RequestForm) PackRequest(DAACRIP0I DAACRIP0I) (responseBody []byte, err error) {

	requestForm := DAACRIP0RequestForm{
		Form: []DAACRIP0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRIP0I",
				},
				FormData: DAACRIP0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACRIP0RequestForm) UnPackRequest(request []byte) (DAACRIP0I, error) {
	DAACRIP0I := DAACRIP0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACRIP0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRIP0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACRIP0ResponseForm) PackResponse(DAACRIP0O DAACRIP0O) (responseBody []byte, err error) {
	responseForm := DAACRIP0ResponseForm{
		Form: []DAACRIP0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRIP0O",
				},
				FormData: DAACRIP0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACRIP0ResponseForm) UnPackResponse(request []byte) (DAACRIP0O, error) {

	DAACRIP0O := DAACRIP0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACRIP0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRIP0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACRIP0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
