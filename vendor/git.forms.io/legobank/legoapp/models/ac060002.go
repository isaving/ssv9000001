package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type AC060002I struct {
	Op                      string		`valid:"Required;MaxSize(1)"`       //操作标志(1-新增，2-修改，3-失效)
	DcnId			  		string
	AcctgAcctNo				string		`valid:"Required;MaxSize(20)"`       //贷款核算账号
	Currency                string											//币种代码
	BalanceType				string											//利息状态
	OrgCreate				string		//开户机构号
	IsCalcInt				string		//是否计息标志
	AcctCreateDate			string		//开户日期
	LastCalcIntDate			string		//上次计息日期
	LastTranIntDate			string		//上次结息日期
	LastWdIntDate           string		//上次计提日期
	WdCyc					string		//计提周期代码
	WdFreq					int64			//计提周期数量
	WdIntFlag				string		//计提利息标识
	IntCyc					string		//计息周期代码
	IntFreq					int64			//计息周期数量
	IntSpeDate				string		//计息指定日
	IntSetlCyc				string		//结息周期代码
	IntSetlFreq				int64			//结息周期频率
	IntSetlSpeDate			string		//结息指定日
	HoliCalcIntFlag			string		//节假日计息标识代码
	PmitBkvtFlag			string		//允许倒起息标识
	PmitDlydBegintFlag      string		//允许延后起息标识
	IsCalcDupInt			string		//是否收取复利标志
	IntSetlPreFlag		    string		//结息优先类型代码
	AcctgStatus				string
	LastTranDate			string
	MgmtOrgId				string
	AcctingOrgId			string
	LastMaintDate			string
	LastMaintTime			string
	LastMaintBrno			string
	LastMaintTell			string
	FrzAmt					float64
	Records					[]AC060002Records
}

type AC060002Records struct {
	BalanceType		string
	IntPlanNo		string
	OrgCreate		string
}

type AC060002O struct {
	AcctgAcctNo				string		//贷款核算账号
	Currency                string											//币种代码
	BalanceType				string											//利息状态
	OrgCreate				string		//开户机构号
	IsCalcInt				string		//是否计息标志
	AcctCreateDate			string		//开户日期
	LastCalcIntDate			string		//上次计息日期
	LastTranIntDate			string		//上次结息日期
	LastWdIntDate           string		//上次计提日期
	WdCyc					string		//计提周期代码
	WdFreq					int64			//计提周期数量
	WdIntFlag				string		//计提利息标识
	IntCyc					string		//计息周期代码
	IntFreq					int64			//计息周期数量
	IntSpeDate				string		//计息指定日
	IntSetlCyc				string		//结息周期代码
	IntSetlFreq				int64			//结息周期频率
	IntSetlSpeDate			string		//结息指定日
	HoliCalcIntFlag			string		//节假日计息标识代码
	PmitBkvtFlag			string		//允许倒起息标识
	PmitDlydBegintFlag      string		//允许延后起息标识
	IsCalcDupInt			string		//是否收取复利标志
	IntSetlPreFlag		    string		//结息优先类型代码
	AcctgStatus				string
	LastTranDate			string
	MgmtOrgId				string
	AcctingOrgId			string
	LastMaintDate			string
	LastMaintTime			string
	LastMaintBrno			string
	LastMaintTell			string
	FrzAmt					float64
	Records					[]AC060002Records
}

type AC060002IDataForm struct {
	FormHead CommonFormHead
	FormData AC060002I
}

type AC060002ODataForm struct {
	FormHead CommonFormHead
	FormData AC060002O
}

type AC060002RequestForm struct {
	Form []AC060002IDataForm
}

type AC060002ResponseForm struct {
	Form []AC060002ODataForm
}

// @Desc Build request message
func (o *AC060002RequestForm) PackRequest(AC060002I AC060002I) (responseBody []byte, err error) {

	requestForm := AC060002RequestForm{
		Form: []AC060002IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC060002I",
				},
				FormData: AC060002I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *AC060002RequestForm) UnPackRequest(request []byte) (AC060002I, error) {
	AC060002I := AC060002I{}
	if err := json.Unmarshal(request, o); nil != err {
		return AC060002I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC060002I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *AC060002ResponseForm) PackResponse(AC060002O AC060002O) (responseBody []byte, err error) {
	responseForm := AC060002ResponseForm{
		Form: []AC060002ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC060002O",
				},
				FormData: AC060002O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *AC060002ResponseForm) UnPackResponse(request []byte) (AC060002O, error) {

	AC060002O := AC060002O{}

	if err := json.Unmarshal(request, o); nil != err {
		return AC060002O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC060002O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *AC060002I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
