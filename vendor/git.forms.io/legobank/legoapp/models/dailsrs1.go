package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAILSRS1I struct {
	SysId            string `json:"sysId"`            //渠道标识 暂定ILNS 乐高智能贷款
	BizSn            string `json:"bizSn"`            //业务流水号
	LoanProdtNo      string `json:"loanProdtNo"`      //产品号
	ApplyScena       string `json:"applyScena"`       //适用场景大类
	TextTmplTypCd    string `json:"textTmplTypCd"`    //模板业务类型代码
	OrgId            string `json:"orgId"`            //机构id
	Area             string `json:"area"`             //地区
	ChannelNumber    string `json:"channelNumber"`    //渠道号
	EventId          string `json:"eventId"`          //事件ID TopicID
	SendObjectType   string `json:"sendObjectType"`   //发送对象类型（01-机构，02-客户经理）
	TextTmplDetailCd string `json:"textTmplDetailCd"` //模板业务类型细类
	MsgDate          string `json:"msgDate"`          //日期
	MsgTime          string `json:"msgTime"`          //时间
}

type DAILSRS1O struct {
	FileId   string `json:"fileId"`
	TotalCnt int    `json:"totalCnt"`
}

func (o *DAILSRS1I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

func (o *DAILSRS1I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

func (o *DAILSRS1O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

func (o *DAILSRS1O) UnPackResponse(responseBody []byte) (err error) {
	commResp := &CommonResponse{
		Data: o,
	}
	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (i *DAILSRS1I) GetMaps() map[string]interface{} {
	maps := make(map[string]interface{})
	requestBody, err := i.PackRequest()
	if err != nil {
		return nil
	}
	if err := json.Unmarshal(requestBody, &maps); nil != err {
		return nil
	}
	return maps
}

func (o *DAILSRS1I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}
