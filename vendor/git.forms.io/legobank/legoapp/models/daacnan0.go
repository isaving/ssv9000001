package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACNAN0I struct {
	BussDate        string
	AcctingDate     string
	AcctingDateTime string
	OrgScenarioId   string
	SrcBizSeqNo     string
	AcctgID         string `validate:"required,max=40"`
	AcctgSeqNo      int		`validate:"required"`
	AcctgSubNo      int		`validate:"required"`
	EntryType       string
	SysId           string
	CoCode          string
	CoCompany       string
	AcctCode        string
	IsOutFlag       string
	Currency        string
	DrCrFlag        string
	Amount          float64
	Account         string
	AcctType        string
	Subject         string
	AcctSubject     string
	AcctDate        string
	SumId           string
	RAcctingNo      string
	IsAcctCenter    string
	IsAcctSucc      string
	InterDate       string
	RAcctFlag       string
	CrAcctFlag      string
	RControFlag     string
	RAcctDate       string
	RAcctNo         string
	CorAcctDate     string
	CorAcctNo       string
	Inputter        string
	AssInputter     string
	AssorInputter   string
	SummaryNo       string
	Summary         string
	Remark          string
	Spare1          string
	Spare2          string
	Spare3          string
	LastMaintDate   string
	LastMaintTime   string
	LastMaintBrno   string
	LastMaintTell   string
	TccState        int
}

type DAACNAN0O struct {
	BussDate        string
	AcctingDate     string
	AcctingDateTime string
	OrgScenarioId   string
	SrcBizSeqNo     string
	AcctgID         string
	AcctgSeqNo      int
	AcctgSubNo      int
	EntryType       string
	SysId           string
	CoCode          string
	CoCompany       string
	AcctCode        string
	IsOutFlag       string
	Currency        string
	DrCrFlag        string
	Amount          float64
	Account         string
	AcctType        string
	Subject         string
	AcctSubject     string
	AcctDate        string
	SumId           string
	RAcctingNo      string
	IsAcctCenter    string
	IsAcctSucc      string
	InterDate       string
	RAcctFlag       string
	CrAcctFlag      string
	RControFlag     string
	RAcctDate       string
	RAcctNo         string
	CorAcctDate     string
	CorAcctNo       string
	Inputter        string
	AssInputter     string
	AssorInputter   string
	SummaryNo       string
	Summary         string
	Remark          string
	Spare1          string
	Spare2          string
	Spare3          string
	LastMaintDate   string
	LastMaintTime   string
	LastMaintBrno   string
	LastMaintTell   string

}

type DAACNAN0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACNAN0I
}

type DAACNAN0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACNAN0O
}

type DAACNAN0RequestForm struct {
	Form []DAACNAN0IDataForm
}

type DAACNAN0ResponseForm struct {
	Form []DAACNAN0ODataForm
}

// @Desc Build request message
func (o *DAACNAN0RequestForm) PackRequest(DAACNAN0I DAACNAN0I) (responseBody []byte, err error) {

	requestForm := DAACNAN0RequestForm{
		Form: []DAACNAN0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACNAN0I",
				},
				FormData: DAACNAN0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACNAN0RequestForm) UnPackRequest(request []byte) (DAACNAN0I, error) {
	DAACNAN0I := DAACNAN0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACNAN0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACNAN0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACNAN0ResponseForm) PackResponse(DAACNAN0O DAACNAN0O) (responseBody []byte, err error) {
	responseForm := DAACNAN0ResponseForm{
		Form: []DAACNAN0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACNAN0O",
				},
				FormData: DAACNAN0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACNAN0ResponseForm) UnPackResponse(request []byte) (DAACNAN0O, error) {

	DAACNAN0O := DAACNAN0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACNAN0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACNAN0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACNAN0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
