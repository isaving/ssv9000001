package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACULR0I struct {
	//输入是个map
}

type DAACULR0O struct {

}

type DAACULR0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACULR0I
}

type DAACULR0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACULR0O
}

type DAACULR0RequestForm struct {
	Form []DAACULR0IDataForm
}

type DAACULR0ResponseForm struct {
	Form []DAACULR0ODataForm
}

// @Desc Build request message
func (o *DAACULR0RequestForm) PackRequest(DAACULR0I DAACULR0I) (responseBody []byte, err error) {

	requestForm := DAACULR0RequestForm{
		Form: []DAACULR0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACULR0I",
				},
				FormData: DAACULR0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACULR0RequestForm) UnPackRequest(request []byte) (DAACULR0I, error) {
	DAACULR0I := DAACULR0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACULR0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACULR0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACULR0ResponseForm) PackResponse(DAACULR0O DAACULR0O) (responseBody []byte, err error) {
	responseForm := DAACULR0ResponseForm{
		Form: []DAACULR0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACULR0O",
				},
				FormData: DAACULR0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACULR0ResponseForm) UnPackResponse(request []byte) (DAACULR0O, error) {

	DAACULR0O := DAACULR0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACULR0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACULR0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACULR0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
