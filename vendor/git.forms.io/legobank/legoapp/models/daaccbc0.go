package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACCBC0I struct {
	AcctgAcctNo    string `validate:"required,max=20"`
	Currency       string
	LoanStatus     string `validate:"required,max=2"`
	AcctType       string
	OrgCreate      string
	AcctCreateDate string
	CurrIntStDate  string
	PeriodNum      int64
	BalanceType    string
	Balance        float64
	CavAmt         float64
	LastTranDate   string
	MgmtOrgId      string
	AcctingOrgId   string
	LastMaintDate  string
	LastMaintTime  string
	LastMaintBrno  string
	LastMaintTell  string
}

type DAACCBC0O struct {
	AcctgAcctNo    string `validate:"required,max=20"`
	Currency       string
	LoanStatus     string `validate:"required,max=2"`
	AcctType       string
	OrgCreate      string
	AcctCreateDate string
	CurrIntStDate  string
	PeriodNum      int64
	BalanceType    string
	Balance        float64
	CavAmt         float64
	LastTranDate   string
	MgmtOrgId      string
	AcctingOrgId   string
	LastMaintDate  string
	LastMaintTime  string
	LastMaintBrno  string
	LastMaintTell  string
}

type DAACCBC0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACCBC0I
}

type DAACCBC0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACCBC0O
}

type DAACCBC0RequestForm struct {
	Form []DAACCBC0IDataForm
}

type DAACCBC0ResponseForm struct {
	Form []DAACCBC0ODataForm
}

// @Desc Build request message
func (o *DAACCBC0RequestForm) PackRequest(DAACCBC0I DAACCBC0I) (responseBody []byte, err error) {

	requestForm := DAACCBC0RequestForm{
		Form: []DAACCBC0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCBC0I",
				},
				FormData: DAACCBC0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACCBC0RequestForm) UnPackRequest(request []byte) (DAACCBC0I, error) {
	DAACCBC0I := DAACCBC0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACCBC0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCBC0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACCBC0ResponseForm) PackResponse(DAACCBC0O DAACCBC0O) (responseBody []byte, err error) {
	responseForm := DAACCBC0ResponseForm{
		Form: []DAACCBC0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCBC0O",
				},
				FormData: DAACCBC0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACCBC0ResponseForm) UnPackResponse(request []byte) (DAACCBC0O, error) {

	DAACCBC0O := DAACCBC0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACCBC0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCBC0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACCBC0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
