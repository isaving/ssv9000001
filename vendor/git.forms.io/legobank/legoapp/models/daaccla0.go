package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACCLA0I struct {
	AcctgAcctNo        string ` validate:"required,max=20"`
	Currency           string
	BalanceType        string
	OrgCreate          string
	IsCalcInt          string
	AcctCreateDate     string
	LastCalcIntDate    string
	LastTranIntDate    string
	LastWdIntDate      string
	WdCyc              string
	WdFreq             int64
	WdIntFlag          string
	IntCyc             string
	IntFreq            int64
	IntSpeDate         string
	IntSetlCyc         string
	IntSetlFreq        int64
	IntSetlSpeDate     string
	HoliCalcIntFlag    string
	PmitBkvtFlag       string
	PmitDlydBegintFlag string
	IsCalcDupInt       string
	IntSetlPreFlag     string
	AcctgStatus        string
	LastTranDate       string
	MgmtOrgId          string
	AcctingOrgId       string
	LastMaintDate      string
	LastMaintTime      string
	LastMaintBrno      string
	LastMaintTell      string
	FrzAmt             float64
}

type DAACCLA0O struct {
	AcctgAcctNo        string ` validate:"required,max=20"`
	Currency           string
	BalanceType        string
	OrgCreate          string
	IsCalcInt          string
	AcctCreateDate     string
	LastCalcIntDate    string
	LastTranIntDate    string
	LastWdIntDate      string
	WdCyc              string
	WdFreq             int64
	WdIntFlag          string
	IntCyc             string
	IntFreq            int64
	IntSpeDate         string
	IntSetlCyc         string
	IntSetlFreq        int64
	IntSetlSpeDate     string
	HoliCalcIntFlag    string
	PmitBkvtFlag       string
	PmitDlydBegintFlag string
	IsCalcDupInt       string
	IntSetlPreFlag     string
	AcctgStatus        string
	LastTranDate       string
	MgmtOrgId          string
	AcctingOrgId       string
	LastMaintDate      string
	LastMaintTime      string
	LastMaintBrno      string
	LastMaintTell      string
	FrzAmt             float64
}

type DAACCLA0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACCLA0I
}

type DAACCLA0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACCLA0O
}

type DAACCLA0RequestForm struct {
	Form []DAACCLA0IDataForm
}

type DAACCLA0ResponseForm struct {
	Form []DAACCLA0ODataForm
}

// @Desc Build request message
func (o *DAACCLA0RequestForm) PackRequest(DAACCLA0I DAACCLA0I) (responseBody []byte, err error) {

	requestForm := DAACCLA0RequestForm{
		Form: []DAACCLA0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCLA0I",
				},
				FormData: DAACCLA0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACCLA0RequestForm) UnPackRequest(request []byte) (DAACCLA0I, error) {
	DAACCLA0I := DAACCLA0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACCLA0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCLA0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACCLA0ResponseForm) PackResponse(DAACCLA0O DAACCLA0O) (responseBody []byte, err error) {
	responseForm := DAACCLA0ResponseForm{
		Form: []DAACCLA0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCLA0O",
				},
				FormData: DAACCLA0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACCLA0ResponseForm) UnPackResponse(request []byte) (DAACCLA0O, error) {

	DAACCLA0O := DAACCLA0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACCLA0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCLA0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACCLA0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
