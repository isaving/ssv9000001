package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DACMRSEQI struct {
	SerialCode	string
}

type DACMRSEQO struct {
	SerialNum	string
}

type DACMRSEQIDataForm struct {
	FormHead CommonFormHead
	FormData DACMRSEQI
}

type DACMRSEQODataForm struct {
	FormHead CommonFormHead
	FormData DACMRSEQO
}

type DACMRSEQRequestForm struct {
	Form []DACMRSEQIDataForm
}

type DACMRSEQResponseForm struct {
	Form []DACMRSEQODataForm
}

// @Desc Build request message
func (o *DACMRSEQRequestForm) PackRequest(DACMRSEQI DACMRSEQI) (responseBody []byte, err error) {

	requestForm := DACMRSEQRequestForm{
		Form: []DACMRSEQIDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DACMRSEQI",
				},
				FormData: DACMRSEQI,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DACMRSEQRequestForm) UnPackRequest(request []byte) (DACMRSEQI, error) {
	DACMRSEQI := DACMRSEQI{}
	if err := json.Unmarshal(request, o); nil != err {
		return DACMRSEQI, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DACMRSEQI, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DACMRSEQResponseForm) PackResponse(DACMRSEQO DACMRSEQO) (responseBody []byte, err error) {
	responseForm := DACMRSEQResponseForm{
		Form: []DACMRSEQODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DACMRSEQO",
				},
				FormData: DACMRSEQO,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DACMRSEQResponseForm) UnPackResponse(request []byte) (DACMRSEQO, error) {

	DACMRSEQO := DACMRSEQO{}

	if err := json.Unmarshal(request, o); nil != err {
		return DACMRSEQO, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DACMRSEQO, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DACMRSEQI) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
