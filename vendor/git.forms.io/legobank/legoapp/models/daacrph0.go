package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACRPH0I struct {
	IntPlanNo	string ` validate:"required,max=20"`
	EffectDate	string ` validate:"required,max=10"`
	ChangeDate	string ` validate:"required,max=10"`
	IntRateUseFlag	string ` validate:"required,max=1"`
}

type DAACRPH0O struct {
	IntPlanNo      string
	Lvls           int64
	BankNo         string
	DepCycle       string
	Dep            int64
	LvlsAmt        float64
	EffectDate     string
	ChangeDate     string
	IntRateUseFlag string
	IntRateNo      string
	IntRateBankNo  string
	IntRate        float64
	FloatOption    string
	FloatFlag      string
	FloatCeil      float64
	FloatFloor     float64
	DefFloatRate   float64
	MinIntRate     float64
	ChangeTell     string
	AuthTeller     string
	Flag1          string
	Flag2          string
	Back1          float64
	Back2          float64
	Back3          string
	Back4          string
	LastMaintDate  string
	LastMaintTime  string
	LastMaintBrno  string
	LastMaintTell  string

}

type DAACRPH0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACRPH0I
}

type DAACRPH0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACRPH0O
}

type DAACRPH0RequestForm struct {
	Form []DAACRPH0IDataForm
}

type DAACRPH0ResponseForm struct {
	Form []DAACRPH0ODataForm
}

// @Desc Build request message
func (o *DAACRPH0RequestForm) PackRequest(DAACRPH0I DAACRPH0I) (responseBody []byte, err error) {

	requestForm := DAACRPH0RequestForm{
		Form: []DAACRPH0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRPH0I",
				},
				FormData: DAACRPH0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACRPH0RequestForm) UnPackRequest(request []byte) (DAACRPH0I, error) {
	DAACRPH0I := DAACRPH0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACRPH0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRPH0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACRPH0ResponseForm) PackResponse(DAACRPH0O DAACRPH0O) (responseBody []byte, err error) {
	responseForm := DAACRPH0ResponseForm{
		Form: []DAACRPH0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRPH0O",
				},
				FormData: DAACRPH0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACRPH0ResponseForm) UnPackResponse(request []byte) (DAACRPH0O, error) {

	DAACRPH0O := DAACRPH0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACRPH0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRPH0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACRPH0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
