package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
)

type DacmdrefRequestForm struct {
	Form []DacmdrefIDataForm `json:"Form"`
}

type DacmdrefIDataForm struct {
	FormHead CommonFormHead `json:"FormHead"`
	FormData DacmdrefI      `json:"FormData"`
}
type DacmdrefI struct {
	TxnPrevDupNo string `json:"TxnPrevDupNo"`
}

type DacmdrefResponseForm struct {
	Form []DacmdrefODataForm `json:"Form"`
}

type DacmdrefODataForm struct {
	FormHead CommonFormHead `json:"FormHead"`
	FormData DacmdrefO      `json:"FormData"`
}
type DacmdrefO struct {
}

// @Desc Parsing request message
func (w *DacmdrefRequestForm) UnPackRequest(req []byte) (DacmdrefI, error) {
	DacmdrefI := DacmdrefI{}
	if err := json.Unmarshal(req, w); nil != err {
		return DacmdrefI, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	if len(w.Form) < 1 {
		return DacmdrefI, errors.Errorf("", "DacmdrefRequestForm.UnPackRequest")
	}

	return w.Form[0].FormData, nil
}

// @Desc Build response message
func (w *DacmdrefResponseForm) PackResponse(dacmdrefOData DacmdrefO) (res []byte, err error) {
	resForm := DacmdrefResponseForm{
		Form: []DacmdrefODataForm{
			DacmdrefODataForm{
				FormHead: CommonFormHead{
					FormId: "DACMDREFO001",
				},
				FormData: dacmdrefOData,
			},
		},
	}

	rspBody, err := json.Marshal(resForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return rspBody, nil
}

// @Desc Build request message
func (w *DacmdrefRequestForm) PackRequest(dacmdrefIData DacmdrefI) (res []byte, err error) {
	requestForm := DacmdrefRequestForm{
		Form: []DacmdrefIDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DACMDREFI001",
				},
				FormData: dacmdrefIData,
			},
		},
	}

	rspBody, err := json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return rspBody, nil
}

// @Desc Parsing response message
func (w *DacmdrefResponseForm) UnPackResponse(req []byte) (DacmdrefO, error) {
	DacmdrefO := DacmdrefO{}

	if err := json.Unmarshal(req, w); nil != err {
		return DacmdrefO, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	if len(w.Form) < 1 {
		return DacmdrefO, errors.Errorf("", "DacmdrefResponseForm.UnPackResponse")
	}

	return w.Form[0].FormData, nil
}
