package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type IL166682IDataForm struct {
	FormHead CommonFormHead
	FormData IL166682I
}

type IL166682ODataForm struct {
	FormHead CommonFormHead
	FormData IL166682O
}

type IL166682RequestForm struct {
	Form []IL166682IDataForm
}

type IL166682ResponseForm struct {
	Form []IL166682ODataForm
}

type IL166682I struct {
	LoanProdtNo string  `validate:"required",json:"LoanProdtNo"` // 产品ID
	CustNo      string  `validate:"required",json:"CustNo"`      // 客户号
	LoanAmt     float64 `json:"LoanAmt"`                         // 贷款金额
	PeriodNum   int     `validate:"required",json:"PeriodNum"`   // 分期期数
	//PeriodType              string  `validate:"required",json:"PeriodType"`      // 还本付息周期
	CustName string `validate:"required",json:"CustName"` // 客户姓名
	//RelaAcctOpnAcctBnkBnkNo string  `json:"RelaAcctOpnAcctBnkBnkNo"`             // 关联账户开户行行号
	GuarMethod      string  `validate:"required",json:"GuarMethod"`      // 担保方式 担保方式1-No Guarantee 2-Guarantor 如果选择1，则担保人姓名，证件类型，证件号码，护照国家都无需输入  如果选择2，则全部都要输入
	GuarName        string  `json:"GuarName"`                            // 担保人姓名
	GuarIdType      string  `json:"GuarIdType"`                          // 担保人证件类型
	GuarIdNo        string  `json:"GuarIdNo"`                            // 担保人证件号码
	GuarPassCountry string  `json:"GuarPassCountry"`                     // 担保护照国家
	ProdName        string  `validate:"required",json:"ProdName"`        // 产品名称
	LoanPur         string  `validate:"required",json:"LoanPur"`         // 贷款目的
	IntRate         float64 `json:"IntRate"`                             // 利率
	CreditLimit     float64 `json:"CreditLimit"`                         // 授信总额度
	CreditUsage     float64 `json:"CreditUsage"`                         // 总额度
	AppDt           string  `validate:"required",json:"AppDt"`           // 申请日期
	DrawDt          string  `validate:"required",json:"DrawDt"`          // 放款日期
	MaturDt         string  `validate:"required",json:"MaturDt"`         // 到期日期
	RepayDay        string  `validate:"required",json:"RepayDay"`        // 还款日
	DisburStatus    string  `validate:"required",json:"DisburStatus"`    // 放款状态
	TransferChannel string  `validate:"required",json:"TransferChannel"` // 放款渠道
	RepayFreq       string  `validate:"required",json:"RepayFreq"`       // 还款周期
	AuotoRepyFlag   string  `validate:"required",json:"AuotoRepyFlag"`   // 自动还款标识
	//RepaymentDt             string  `validate:"required",json:"RepaymentDt"`     // 还款日期
	AppFee         float64 `json:"AppFee"`                           // 申请费用
	ProcFee        float64 `json:"ProcFee"`                          // 处理费
	OverFee        float64 `json:"OverFee"`                          // 逾期费用
	AnticipatedFee float64 `json:"AnticipatedFee"`                   // 提前还款费用
	ExtensionFee   float64 `json:"ExtensionFee"`                     // 展期费用
	RepayPlanFee   float64 `json:"RepayPlanFee"`                     // 还款计划调整费用
	Tax            float64 `json:"Tax"`                              // 税
	Maker          string  `validate:"required",json:"Maker"`        // 申请人员工号
	MakerComment   string  `validate:"required",json:"MakerComment"` // 放款说明
	MakelnAplySn   string  `json:"MakelnAplySn"`                     // 放款申请流水号
}

type IL166682O struct {
	Msg          string `json:"Msg"`          // 返回信息
	MakelnAplySn string `json:"MakelnAplySn"` // 放款申请流水号
}

// @Desc Build request message
func (o *IL166682RequestForm) PackRequest(il166682I IL166682I) (responseBody []byte, err error) {

	requestForm := IL166682RequestForm{
		Form: []IL166682IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "IL166682I",
				},
				FormData: il166682I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *IL166682RequestForm) UnPackRequest(request []byte) (IL166682I, error) {
	il166682I := IL166682I{}
	if err := json.Unmarshal(request, o); nil != err {
		return il166682I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return il166682I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *IL166682ResponseForm) PackResponse(il166682O IL166682O) (responseBody []byte, err error) {
	responseForm := IL166682ResponseForm{
		Form: []IL166682ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "IL166682O",
				},
				FormData: il166682O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *IL166682ResponseForm) UnPackResponse(request []byte) (IL166682O, error) {

	il166682O := IL166682O{}

	if err := json.Unmarshal(request, o); nil != err {
		return il166682O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return il166682O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *IL166682I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
