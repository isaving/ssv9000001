package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAAC0005I struct {

}

type DAAC0005O struct {

}

type DAAC0005IDataForm struct {
	FormHead CommonFormHead
	FormData DAAC0005I
}

type DAAC0005ODataForm struct {
	FormHead CommonFormHead
	FormData DAAC0005O
}

type DAAC0005RequestForm struct {
	Form []DAAC0005IDataForm
}

type DAAC0005ResponseForm struct {
	Form []DAAC0005ODataForm
}

// @Desc Build request message
func (o *DAAC0005RequestForm) PackRequest(DAAC0005I DAAC0005I) (responseBody []byte, err error) {

	requestForm := DAAC0005RequestForm{
		Form: []DAAC0005IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAAC0005I",
				},
				FormData: DAAC0005I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAAC0005RequestForm) UnPackRequest(request []byte) (DAAC0005I, error) {
	DAAC0005I := DAAC0005I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAAC0005I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAAC0005I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAAC0005ResponseForm) PackResponse(DAAC0005O DAAC0005O) (responseBody []byte, err error) {
	responseForm := DAAC0005ResponseForm{
		Form: []DAAC0005ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAAC0005O",
				},
				FormData: DAAC0005O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAAC0005ResponseForm) UnPackResponse(request []byte) (DAAC0005O, error) {

	DAAC0005O := DAAC0005O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAAC0005O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAAC0005O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAAC0005I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
