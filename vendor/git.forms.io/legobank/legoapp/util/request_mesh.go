package util

import (
	"git.forms.io/legobank/legoapp/config"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/models"
	"git.forms.io/universe/comm-agent/client"
	common "git.forms.io/universe/comm-agent/common/protocol"
	"git.forms.io/universe/solapp-sdk/comm"
	"git.forms.io/universe/solapp-sdk/compensable"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego"
	jsoniter "github.com/json-iterator/go"
	"github.com/pkg/errors"
	"strings"
)

var remoteCall = comm.NewRemoteClientFactory().CreateClient()
var json = jsoniter.ConfigCompatibleWithStandardLibrary

func SendRequestToExternalSystem(sourceRequest *client.UserMessage, srcMap map[string]string, serviceKey string, formData map[string]interface{}, c *compensable.TxnCtx) (map[string]interface{}, error) {
	log.Debugf("start request external system: %s, data: %v", serviceKey, formData)
	rspMap := make(map[string]interface{})
	response := &client.UserMessage{}
	repFormId := beego.AppConfig.String("requestForm::" + serviceKey)
	request, err := BuildMessage(sourceRequest, repFormId, formData)
	if err != nil {
		return rspMap, err
	}
	timeout := 60
	setHeader(request, srcMap)
	request.AppProps[constant.SRCBIZSEQNO] = getBusinessSerialNo()

	dcn := config.ServiceConf.GroupDcn
	if dcn == "000000" {
		dcn = ""
	}
	if elemType, ok := request.AppProps[common.DLS_ELEMENT_TYPE]; !ok || elemType == "" {
		request.AppProps[common.DLS_ELEMENT_TYPE] = "CMM"
	}
	if elemId, ok := request.AppProps[common.DLS_ELEMENT_ID]; !ok || elemId == "" {
		request.AppProps[common.DLS_ELEMENT_ID] = "common"
	}

	log.Debugf("start remoteCall request [%++v]", PrintUserMsg(request))
	if err := remoteCall.SyncCall(c, config.ServiceConf.Organization, dcn, serviceKey, request, response, timeout); err != nil {
		log.Errorf("remoteCall failed error [%v]", err)
		return rspMap, err
	}
	log.Debugf("end remoteCall request [%++v]", PrintUserMsg(response))
	rspMap, err = ParseMessage(response)
	updateRequest(sourceRequest, response)
	if err != nil {
		return rspMap, err
	}
	return rspMap, nil
}

func SendRequestToExternalSystemAny(sourceRequest *client.UserMessage, srcMap map[string]string, serviceKey string, formData interface{}, c *compensable.TxnCtx) (string, error) {
	log.Debugf("start request external system: %s, data: %v", serviceKey, formData)

	response := &client.UserMessage{}
	repFormId := beego.AppConfig.String("requestForm::" + serviceKey)

	request, err := BuildMessageAny(sourceRequest, repFormId, formData)
	if err != nil {
		return "", err
	}
	timeout := 60
	setHeader(request, srcMap)
	request.AppProps[constant.SRCBIZSEQNO] = getBusinessSerialNo()

	dcn := config.ServiceConf.GroupDcn
	if dcn == "000000" {
		dcn = ""
	}
	if elemType, ok := request.AppProps[common.DLS_ELEMENT_TYPE]; !ok || elemType == "" {
		request.AppProps[common.DLS_ELEMENT_TYPE] = "CMM"
	}
	if elemId, ok := request.AppProps[common.DLS_ELEMENT_ID]; !ok || elemId == "" {
		request.AppProps[common.DLS_ELEMENT_ID] = "common"
	}

	log.Debugf("start remoteCall request [%++v]", PrintUserMsg(request))
	if err := remoteCall.SyncCall(c, config.ServiceConf.Organization, dcn, serviceKey, request, response, timeout); err != nil {
		log.Errorf("remoteCall failed error [%v]", err)
		return "", err
	}
	log.Debugf("end remoteCall request [%++v]", PrintUserMsg(response))
	resp, err := ParseMessageAny(response)
	updateRequest(sourceRequest, response)
	if err != nil {
		return "", err
	}
	return resp, nil
}

func SendRequestToBackward(sourceRequest *client.UserMessage, srcMap map[string]string, serviceKey string, body []byte, c *compensable.TxnCtx) ([]byte, error) {
	log.Debugf("start request external system: %s, data: %v", serviceKey, string(body))

	response := &client.UserMessage{}
	repFormId := beego.AppConfig.String("requestForm::" + serviceKey)
	request, err := BuildBackwardMessage(sourceRequest, repFormId, body)
	if err != nil {
		return []byte{}, err
	}
	timeout := 60
	setHeader(request, srcMap)
	request.AppProps[constant.SRCBIZSEQNO] = getBusinessSerialNo()

	dcn := config.ServiceConf.GroupDcn
	if dcn == "000000" {
		dcn = ""
	}
	if elemType, ok := request.AppProps[common.DLS_ELEMENT_TYPE]; !ok || elemType == "" {
		request.AppProps[common.DLS_ELEMENT_TYPE] = "CMM"
	}
	if elemId, ok := request.AppProps[common.DLS_ELEMENT_ID]; !ok || elemId == "" {
		request.AppProps[common.DLS_ELEMENT_ID] = "common"
	}
	request.AppProps[common.TARGET_DCN] = ""

	log.Debugf("start remoteCall request [%++v]", PrintUserMsg(request))
	if err := remoteCall.SyncCall(c, config.ServiceConf.Organization, dcn, serviceKey, request, response, timeout); err != nil {
		log.Errorf("remoteCall failed error [%v]", err)
		return nil, err
	}
	log.Debugf("end remoteCall request [%++v]", PrintUserMsg(response))
	rspBody, err := ParseBackwardMessage(response)
	updateRequest(sourceRequest, response)
	if err != nil {
		return rspBody, err
	}
	return rspBody, nil
}

func SendRequestToExternalSystemRetry(sourceRequest *client.UserMessage, srcMap map[string]string, serviceKey string, formData map[string]interface{}, c *compensable.TxnCtx) (map[string]interface{}, error) {
	log.Debugf("start request external system: %s, data: %v", serviceKey, formData)
	rspMap := make(map[string]interface{})
	response := &client.UserMessage{}
	repFormId := beego.AppConfig.String("requestForm::" + serviceKey)
	request, err := BuildMessage(sourceRequest, repFormId, formData)
	if err != nil {
		return rspMap, err
	}
	setHeader(request, srcMap)
	request.AppProps[constant.SRCBIZSEQNO] = getBusinessSerialNo()

	dcn := config.ServiceConf.GroupDcn
	if dcn == "000000" {
		dcn = ""
	}
	if elemType, ok := request.AppProps[common.DLS_ELEMENT_TYPE]; !ok || elemType == "" {
		request.AppProps[common.DLS_ELEMENT_TYPE] = "CMM"
	}
	if elemId, ok := request.AppProps[common.DLS_ELEMENT_ID]; !ok || elemId == "" {
		request.AppProps[common.DLS_ELEMENT_ID] = "common"
	}
	request.AppProps[common.TARGET_DCN] = ""

	log.Debugf("start remoteCall request [%++v]", PrintUserMsg(request))

	maxRetryTimes := beego.AppConfig.DefaultInt(serviceKey+"::maxRetryTimes", 0)
	maxAutoRetryTimes := beego.AppConfig.DefaultInt(serviceKey+"::maxAutoRetryTimes", 0)
	timeoutMilliseconds := beego.AppConfig.DefaultInt(serviceKey+"::timeoutMilliseconds", 30000)

	tryCnt := 0
	for {
		tryCnt++
		err = remoteCall.SyncCallV2(c, config.ServiceConf.Organization, dcn, serviceKey, request, response, timeoutMilliseconds, maxAutoRetryTimes)
		log.Debugf("end remoteCall request [%++v]", PrintUserMsg(response))
		if comm.ErrTimeout == err {
			if tryCnt <= maxRetryTimes {
				log.Errorf("Request target service [%s] timeout, try counts [%d], try again, GlobalBizSeqNo [%s], SrcBizSeqNo [%s]...",
					serviceKey, tryCnt, request.AppProps[constant.GLOBALBIZSEQNO], request.AppProps[constant.SRCBIZSEQNO])
				continue
			} else {
				log.Errorf("Request target service [%s] failed error [%v], GlobalBizSeqNo [%s], SrcBizSeqNo [%s]",
					serviceKey, err, request.AppProps[constant.GLOBALBIZSEQNO], request.AppProps[constant.SRCBIZSEQNO])
				return rspMap, err
			}
		} else if err != nil {
			log.Errorf("Request target service [%s] failed error [%v], GlobalBizSeqNo [%s], SrcBizSeqNo [%s]",
				serviceKey, err, request.AppProps[constant.GLOBALBIZSEQNO], request.AppProps[constant.SRCBIZSEQNO])
			return rspMap, err
		} else {
			retMsgCode := response.AppProps[constant.RETMSGCODE]
			if retMsgCode == "" {
				break
			}
			if retMsgCode != constant.SYTIMEOUT &&
				retMsgCode != constant.SYERRCONT {
				break
			}
			if tryCnt <= maxRetryTimes {
				log.Errorf("Request target service [%s] timeout, try counts [%d], try again, GlobalBizSeqNo [%s], SrcBizSeqNo [%s]...",
					serviceKey, tryCnt, request.AppProps[constant.GLOBALBIZSEQNO], request.AppProps[constant.SRCBIZSEQNO])
				continue
			} else {
				log.Errorf("Request target service [%s] failed error [%v], GlobalBizSeqNo [%s], SrcBizSeqNo [%s]",
					serviceKey, err, request.AppProps[constant.GLOBALBIZSEQNO], request.AppProps[constant.SRCBIZSEQNO])
				break
			}
		}
	}

	rspMap, err = ParseMessage(response)
	updateRequest(sourceRequest, response)
	if err != nil {
		return rspMap, err
	}
	return rspMap, nil
}

func SendSemiRequestToExternalSystem(sourceRequest *client.UserMessage, srcMap map[string]string, serviceKey string, formData map[string]interface{}, c *compensable.TxnCtx) (map[string]interface{}, error) {
	log.Debugf("start Semi request external system: %s, data: %v", serviceKey, formData)
	rspMap := make(map[string]interface{})
	response := &client.UserMessage{}
	repFormId := beego.AppConfig.String("requestForm::" + serviceKey)
	request, err := BuildMessage(sourceRequest, repFormId, formData)
	if err != nil {
		return rspMap, err
	}
	timeout := 60
	setHeader(request, srcMap)
	request.AppProps[constant.SRCBIZSEQNO] = getBusinessSerialNo()

	dcn := config.ServiceConf.GroupDcn
	if dcn == "000000" {
		dcn = ""
	}
	if elemType, ok := request.AppProps[common.DLS_ELEMENT_TYPE]; !ok || elemType == "" {
		request.AppProps[common.DLS_ELEMENT_TYPE] = "CMM"
	}
	if elemId, ok := request.AppProps[common.DLS_ELEMENT_ID]; !ok || elemId == "" {
		request.AppProps[common.DLS_ELEMENT_ID] = "common"
	}
	request.AppProps[common.TARGET_DCN] = ""

	log.Debugf("start remoteCall request [%++v]", PrintUserMsg(request))
	if err := remoteCall.SemiSyncCall(c, config.ServiceConf.Organization, dcn, serviceKey, request, response, timeout); err != nil {
		log.Errorf("remoteCall failed error [%v]", err)
		return rspMap, err
	}
	log.Debugf("end remoteCall request [%++v]", PrintUserMsg(response))
	rspMap, err = ParseMessage(response)
	updateRequest(sourceRequest, response)
	if err != nil {
		return rspMap, err
	}
	return rspMap, nil
}

func SendRequestToExternalSystemArr(sourceRequest *client.UserMessage, srcMap map[string]string, serviceKey string, formData []map[string]interface{}, c *compensable.TxnCtx) ([]map[string]interface{}, error) {
	log.Debugf("start request external system: %s, data: %v", serviceKey, formData)
	rspMap := make([]map[string]interface{}, 0)
	response := &client.UserMessage{}
	repFormId := beego.AppConfig.String("requestForm::" + serviceKey)
	request, err := BuildMessageArr(sourceRequest, repFormId, formData)
	if err != nil {
		return rspMap, err
	}
	timeout := 60
	setHeader(request, srcMap)
	request.AppProps[constant.SRCBIZSEQNO] = getBusinessSerialNo()

	dcn := config.ServiceConf.GroupDcn
	if dcn == "000000" {
		dcn = ""
	}
	if elemType, ok := request.AppProps[common.DLS_ELEMENT_TYPE]; !ok || elemType == "" {
		request.AppProps[common.DLS_ELEMENT_TYPE] = "CMM"
	}
	if elemId, ok := request.AppProps[common.DLS_ELEMENT_ID]; !ok || elemId == "" {
		request.AppProps[common.DLS_ELEMENT_ID] = "common"
	}

	request.AppProps[common.TARGET_DCN] = ""

	log.Debugf("start remoteCall request [%++v]", PrintUserMsg(request))

	if err := remoteCall.SyncCall(c, config.ServiceConf.Organization, dcn, serviceKey, request, response, timeout); err != nil {
		log.Errorf("remoteCall failed error [%v]", err)
		return rspMap, err
	}

	log.Debugf("end remoteCall request [%++v]", PrintUserMsg(response))

	rspMap, err = ParseMessageArr(response)
	updateRequest(sourceRequest, response)
	if err != nil {
		return rspMap, err
	}

	return rspMap, nil
}

func SendAsyncRequestToExternalSystem(sourceRequest *client.UserMessage, srcMap map[string]string, serviceKey string, formData map[string]interface{}, c *compensable.TxnCtx) error {
	log.Debugf("start asynchronous request external system: %s, data: %v, ctx: [%v]", serviceKey, formData, c)

	repFormId := beego.AppConfig.String("requestForm::" + serviceKey)
	request, err := BuildMessage(sourceRequest, repFormId, formData)
	if err != nil {
		return err
	}

	setHeader(request, srcMap)
	request.AppProps[constant.SRCBIZSEQNO] = getBusinessSerialNo()
	dcn := config.ServiceConf.GroupDcn
	if dcn == "000000" {
		dcn = ""
	}
	if elemType, ok := request.AppProps[common.DLS_ELEMENT_TYPE]; !ok || elemType == "" {
		request.AppProps[common.DLS_ELEMENT_TYPE] = "CMM"
	}
	if elemId, ok := request.AppProps[common.DLS_ELEMENT_ID]; !ok || elemId == "" {
		request.AppProps[common.DLS_ELEMENT_ID] = "common"
	}
	request.AppProps[common.TARGET_DCN] = ""

	log.Debugf("start remote Async Call request [%++v]", PrintUserMsg(request))
	if err := remoteCall.AsyncCall(c, config.ServiceConf.Organization, dcn, serviceKey, request); err != nil {
		log.Errorf("remote Async Call failed error [%v]", err)
		return err
	}

	return nil
}

func BuildMessage(sourceRequest *client.UserMessage, repFormId string, formData map[string]interface{}) (*client.UserMessage, error) {
	body := models.Body{
		Form: []models.Form{
			{
				FormHead: models.FormHead{
					FormId: repFormId,
				},
				FormData: formData,
			},
		},
	}
	bodyBuf, err := json.Marshal(body)
	if err != nil {
		return &client.UserMessage{}, err
	}
	request := &client.UserMessage{
		Id:             sourceRequest.Id,
		TopicAttribute: sourceRequest.TopicAttribute,
		NeedReply:      sourceRequest.NeedReply,
		NeedAck:        sourceRequest.NeedAck,
		AppProps:       sourceRequest.AppProps,
		Body:           bodyBuf,
	}
	return request, nil
}

func BuildMessageAny(sourceRequest *client.UserMessage, repFormId string, formData interface{}) (*client.UserMessage, error) {
	body := models.BodyMult{
		Form: []models.FormMult{
			{
				FormHead: models.FormHead{
					FormId: repFormId,
				},
				FormData: formData,
			},
		},
	}
	bodyBuf, err := json.Marshal(body)
	if err != nil {
		return &client.UserMessage{}, err
	}
	request := &client.UserMessage{
		Id:             sourceRequest.Id,
		TopicAttribute: sourceRequest.TopicAttribute,
		NeedReply:      sourceRequest.NeedReply,
		NeedAck:        sourceRequest.NeedAck,
		AppProps:       sourceRequest.AppProps,
		Body:           bodyBuf,
	}
	return request, nil
}

func BuildBackwardMessage(sourceRequest *client.UserMessage, repFormId string, body []byte) (*client.UserMessage, error) {

	request := &client.UserMessage{
		Id:             sourceRequest.Id,
		TopicAttribute: sourceRequest.TopicAttribute,
		NeedReply:      sourceRequest.NeedReply,
		NeedAck:        sourceRequest.NeedAck,
		AppProps:       sourceRequest.AppProps,
		Body:           body,
	}
	return request, nil
}

func BuildMessageArr(sourceRequest *client.UserMessage, repFormId string, formData []map[string]interface{}) (*client.UserMessage, error) {
	body := models.BodyArr{
		Form: []models.FormArr{
			{
				FormHead: models.FormHead{
					FormId: repFormId,
				},
				FormData: formData,
			},
		},
	}
	bodyBuf, err := json.Marshal(body)
	if err != nil {
		return &client.UserMessage{}, err
	}
	request := &client.UserMessage{
		Id:             sourceRequest.Id,
		TopicAttribute: sourceRequest.TopicAttribute,
		NeedReply:      sourceRequest.NeedReply,
		NeedAck:        sourceRequest.NeedAck,
		AppProps:       sourceRequest.AppProps,
		Body:           bodyBuf,
	}
	return request, nil
}

func ParseMessage(response *client.UserMessage) (map[string]interface{}, error) {
	log.Debug("start parse message")
	rspMap := make(map[string]interface{})

	bodyBuf := response.Body
	body := models.Body{}
	err := json.Unmarshal(bodyBuf, &body)
	if err != nil {
		return rspMap, err
	}
	if len(body.Form) < 1 {
		return rspMap, errors.Errorf("UserMessage body illegal")
	}
	rspMap = body.Form[0].FormData

	if response.AppProps[constant.RETSTATUS] == "F" {
		return rspMap, errors.Errorf("%v", rspMap[constant.RETMESSAGE])
	}

	if body.Form[0].FormHead.FormId == constant.ERRORFORMID {
		return rspMap, errors.Errorf("%v", rspMap[constant.RETMESSAGE])
	}
	return rspMap, nil
}

func ParseBackwardMessage(response *client.UserMessage) ([]byte, error) {

	return response.Body, nil

}

func ParseMessageArr(response *client.UserMessage) ([]map[string]interface{}, error) {
	log.Debug("start parse message")
	rspMaps := make([]map[string]interface{}, 0)

	bodyBuf := response.Body
	body := models.BodyMult{}
	err := json.Unmarshal(bodyBuf, &body)
	if err != nil {
		return rspMaps, err
	}
	if len(body.Form) < 1 {
		return rspMaps, errors.Errorf("UserMessage body illegal")
	}
	rspData := body.Form[0].FormData
	switch data := rspData.(type) {
	case interface{}:
		rspMaps = append(rspMaps, data.(map[string]interface{}))
	case []interface{}:
		for _, rspMap := range data {
			rspMaps = append(rspMaps, rspMap.(map[string]interface{}))
		}
	}

	if response.AppProps[constant.RETSTATUS] == "F" {
		return rspMaps, errors.Errorf("%v", rspMaps[0][constant.RETMESSAGE])
	}

	if body.Form[0].FormHead.FormId == constant.ERRORFORMID {
		return rspMaps, errors.Errorf("%v", rspMaps[0][constant.RETMESSAGE])
	}
	return rspMaps, nil
}

func ParseMessageAny(msg *client.UserMessage) (formData string, err error) {

	log.Debugf("Start parse message: %++v", msg)

	bodyRaw := msg.Body
	body := &models.BodyMult{}
	if err := json.Unmarshal(bodyRaw, body); err != nil {
		return "", err
	}

	if len(body.Form) < 1 {
		return "", errors.New("UserMessage body illegal, not found Form")
	}

	formData = json.Get(bodyRaw, "Form", 0, "FormData").ToString()

	if msg.AppProps[constant.RETSTATUS] == "F" || body.Form[0].FormHead.FormId == constant.ERRORFORMID {

		if strings.HasPrefix(formData, "[") {

			errCode := json.Get(bodyRaw, "Form", 0, "FormData", 0, constant.RETMSGCODE).ToString()
			errMsg := json.Get(bodyRaw, "Form", 0, "FormData", 0, constant.RETMESSAGE).ToString()

			return "", errors.Errorf("%s: %s", errCode, errMsg)
		} else {

			errCode := json.Get(bodyRaw, "Form", 0, "FormData", constant.RETMSGCODE).ToString()
			errMsg := json.Get(bodyRaw, "Form", 0, "FormData", constant.RETMESSAGE).ToString()
			return "", errors.Errorf("%s: %s", errCode, errMsg)
		}

	}

	return formData, nil
}

func updateRequest(request *client.UserMessage, response *client.UserMessage) {
	request.AppProps[constant.TRGBIZSEQNO] = response.AppProps[constant.SRCBIZSEQNO]
	request.AppProps[constant.LASTACTENTRYNO] = response.AppProps[constant.LASTACTENTRYNO]
	request.AppProps[common.DLS_ELEMENT_TYPE] = ""
	request.AppProps[common.DLS_ELEMENT_ID] = ""
}
