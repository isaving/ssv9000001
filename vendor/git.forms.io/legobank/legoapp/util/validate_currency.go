package util

import (
	"fmt"
	"git.forms.io/legobank/legoapp/constant"
)

func ValidateCurrency(currency string) error {

	if _, existed := constant.ISO4217List[currency]; !existed {
		return fmt.Errorf("currency illegal")
	}
	return nil
}
