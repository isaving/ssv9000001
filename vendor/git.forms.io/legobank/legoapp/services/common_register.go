package services

import (
	"fmt"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/util"
	"git.forms.io/universe/comm-agent/client"
	dtsClient "git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"git.forms.io/universe/solapp-sdk/compensable"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/pkg/errors"
	"runtime/debug"
)

func CallDts(service interface{}, compens dtsClient.Compensable, request *client.UserMessage, c *compensable.TxnCtx) (response *client.UserMessage) {

	response = &client.UserMessage{}
	defer func() {
		var err error
		if r := recover(); r != nil {
			switch x := r.(type) {
			case string:
				err = errors.New(x)
			case error:
				err = x
			default:
				err = errors.New("")
			}
			log.Errorf("catch panic %v", err)
			log.Error(fmt.Sprintf("%s", debug.Stack()))
			response, _ = util.BuildErrorResponse(request, nil, constant.SYSCODE1, err.Error())
		}
	}()

	proxy, err := aspect.NewDTSProxy(service, compens, c)
	if err != nil {
		log.Error("Register DTS Proxy failed.")
		return response
	}

	//log.Infof("dts request: %++v", util.PrintUserMsg(request))
	log.Infof_V4("dts request: %++v", request)
	rets := proxy.Do(request)
	if rets[len(rets)-1].Interface() != nil {
		log.Errorf("Proxy execute failed %v", rets[len(rets)-1].Interface())
		rsp := rets[0].Interface()
		switch rsp.(type) {
		case *client.UserMessage:
			response = rsp.(*client.UserMessage)
		}
	} else {
		response = rets[0].Interface().(*client.UserMessage)
	}
	//log.Infof("dts response: %++v", util.PrintUserMsg(response))
	log.Infof_V4("dts response: %++v", response)

	return response

}
