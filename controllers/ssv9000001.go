//Version: v0.0.1
package controllers

import (
	"encoding/json"
	"git.forms.io/isaving/sv/ssv9000001/models"
	"git.forms.io/isaving/sv/ssv9000001/dao"
	"git.forms.io/isaving/sv/ssv9000001/services"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/dts/client/aspect"
	"git.forms.io/universe/solapp-sdk/log"
	"runtime/debug"
)

type Ssv9000001Controller struct {
    controllers.CommTCCController
}

func (*Ssv9000001Controller) ControllerName() string {
	return "Ssv9000001Controller"
}

// @Desc Ac020001Controller Controller
// @Description Entry
// @Param ac020001 body models.SSV9000001I true "body for model content"
// @Success 200 {object} models.SSV9000001O
// @router /ssv9000001 [post]
// @Date 2020-11-25
func (c *Ssv9000001Controller) Ssv9000001() {

	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv9000001Controller.Ssv9000001 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()

	ssv9000001I := &models.SSV9000001I{}
	if err :=  models.UnPackRequest(c.Req.Body, ssv9000001I); err != nil {
		c.SetServiceError(err)
		return
	}
  if err := ssv9000001I.Validate(); err != nil {
		log.Errorf("Request message field validate failed, error:[%v]", err)
		c.SetServiceError(err)
		return
	}
	ssv9000001 := &services.Ssv9000001Impl{} 
   	ssv9000001.New(c.CommTCCController)
  	ssv9000001.SSV9000001I = ssv9000001I

	//ssv9000001.DlsInterface = &commclient.DlsOperate{}
	ssv9000001Compensable := services.Ssv9000001Compensable

	proxy, err := aspect.NewDTSProxy(ssv9000001, ssv9000001Compensable, c.DTSCtx)
	if err != nil {
		log.Errorf("Register DTS Proxy failed. %v", err)
		c.SetServiceError(errors.Errorf(constant.PROXYREGFAILD, "Register DTS Proxy failed. %v", err))
		return
	}

	rets := proxy.Do(ssv9000001I)

	if len(rets) < 1 {
		log.Error("DTS proxy executed service failed, not have any return")
		c.SetServiceError(errors.Errorf(constant.PROXYFAILD,"DTS proxy executed service failed, %v", "not have any return"))
		return
	}

	if e := rets[len(rets)-1].Interface(); e != nil {
		log.Errorf("DTS proxy executed service failed %v", err)
		c.SetServiceError(e)
		return
	}

	rsp := rets[0].Interface()
	if ssv9000001O, ok := rsp.(*models.SSV9000001O); ok {
		if responseBody, err := models.PackResponse(ssv9000001O); err != nil {
			c.SetServiceError(err)
		} else {
			c.SetAppBody(responseBody)
		}
	}
} 
// @Title Ssv9000001 Controller
// @Description ssv9000001 controller
// @Param Ssv9000001 body models.SSV9000001I true body for SSV9000001 content
// @Success 200 {object} models.SSV9000001O
// @router /create [post]
func (c *Ssv9000001Controller) SWSsv9000001() {
	//Here is to generate API documentation, no need to implement methods
}

func (c *Ssv9000001Controller) DASV0001() {
	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv9000001Controller.Ssv9000001 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()
	log.Infof("c.SrcAppProps:%v",c.Req.AppProps["Topic"],)

	DataMap := make(map[string]interface{})
	if "" == c.Req.AppProps["Topic"]  {
		DataMap["Message"] = "Header Topic Is NUll"
		responseBody, _ := json.Marshal(DataMap)
		c.SetAppBody(responseBody)
		return
	}

	var TJson dao.TJson
	if _, err :=dao.EngineCache.Where("topic_id=?",c.Req.AppProps["Topic"]).Get(&TJson); nil != err {
		DataMap["Message"] = err
		responseBody, _ := json.Marshal(DataMap)
		c.SetAppBody(responseBody)
		return
	}

	if "" == TJson.Json {
		DataMap["Message"] = "Not Records"
		responseBody, _ := json.Marshal(DataMap)
		c.SetAppBody(responseBody)
		return
	}

	if err := json.Unmarshal([]byte(TJson.Json), &DataMap); nil != err {
		DataMap["Message"] = "Unmarshal Faild"
		responseBody, _ := json.Marshal(DataMap)
		c.SetAppBody(responseBody)
		return
	}

	//commResp := &models.CommonResponse{
	//	ReturnCode: "0",
	//	ReturnMsg: "success",
	//	Data: DataMap,
	//}
	log.Infof("commResp: %v", DataMap)
	responseBody, _ := json.Marshal(DataMap)
	c.SetAppBody(responseBody)
}