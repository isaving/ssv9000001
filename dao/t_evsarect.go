package dao

import "git.forms.io/universe/solapp-sdk/log"

type TEvsarect struct {
	AgreementId    string    //合约号
	AgreementType  string    //合约类型
	BnAcc          string    //开户行所
	CrcanclDate    string    //开/销户日期
	TransationDate string    //交易日期
	GlobalBizSeqNo string    //全局流水号
	SrcBizSeqNo    string    //服务流水号
	RecordType     string    //记录类型
	Currency       string    //币种
	CstmrId        string    //客户编号
	CstmrTyp       string    //客户类型
	AccuntNme      string    //账户中文名称
	PrductId       string    //产品号
	PrductNm       int       //产品名称
	TranAccType    string    //账户类型 1-交易账户;2-银行账户-
	DepositNature  string    //账户性质 00-基本账户;01-一般账户
	MediumType     string    //开户介质类型
	MediumNm       string    //开户介质号码
	CrtAgreemntAmt float64   //开户金额
	CrtcanlEm      string    //开/销户柜员
	CrtcanlApEm    string    //开/销户授权柜员
	CancelReason   string    //销户原因
	ContactsName   string    //联系人姓名
	ContactsId     string    //联系人id
	ContactsIdnm   string    //联系人姓名
	ContactsPh     string    //联系人电话1
	ContactsAd     string    //联系人地址1
	RecordStatus   string    //有效标志 Y-有效;N-无效;A-全部;-
	LastUpDatetime string    //
	LastUpBn       string    //
	LastUpEm       string    //
	TccStatus      string    //
}
func (t *TEvsarect) TableName() string {
	return "t_evsarect"
}

func Insert_t_evsarect(Para *TEvsarect) error{
	if _, err := EngineCache.Omit().Insert(Para); nil != err {
		log.Errorf("Insert TPersonArgeement, err: %s",err)
		return  err
	}

	return nil
}
