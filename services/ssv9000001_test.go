package services

import (
	"encoding/json"
	"fmt"
	"git.forms.io/isaving/sv/ssv9000001/dao"
	"git.forms.io/isaving/sv/ssv9000001/models"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/astaxie/beego"
	_ "github.com/go-sql-driver/mysql"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/xormplus/xorm"
	"os"
	"testing"
	"time"
)

func TestMain(m *testing.M) {
	_ = beego.LoadAppConfig("ini", "../conf/app.conf")
	_ = dao.InitDatabase()
	exitVal := m.Run()
	os.Exit(exitVal)
}


func TestEul20001Impl_Eul20001_Success(t *testing.T) {
	input := &models.SSV9000001I{
		AgreementId : "1",
		PrductNm : 2,
		AgreementType : "1",
		Currency : "1",
		CstmrId : "1",
		CstmrTyp : "1",
		AccuntNme : "1",
		UsgCod : "1",
		WdrwlMthd : "1",
		AccPsw : "1",
		TermFlag : "1",
		BegnTxDt : "1",
		AppntDtFlg : "1",
		AppntDt : "1",
		AccCnclFlg : "1",
		AutCnl : "1",
		SttmntFlg : "1",
		WthdrwlMthd : "1",
		OvrDrwFlg : "1",
		CstmrCntctAdd : "1",
		CstmrCntctPh : "1",
		CstmrCntctEm : "1",
		BnCrtAcc : "1",
		AccOpnDt : "1",
	}

	impl := Ssv9000001Impl{
		SSV9000001I: input,
		O:         dao.EngineCache,
	}

	Convey("Call service", t, func() {
		//var session *dao.Session
		Convey("Mock data", func() {
			db, mock, err := sqlmock.New()
			So(err, ShouldBeNil)
			dao.EngineCache.DB().DB = db

			mock.ExpectExec("t_person_argeement").WithArgs(input).WithArgs()
			mock.ExpectExec("t_evsarect").WithArgs(input).WithArgs()

			mock.ExpectQuery("t_person_argeement").WillReturnRows(sqlmock.NewRows([]string{"agreement_Id"}).AddRow("1000001"))
			Convey("Validate session", func() {
				_, _ = impl.TrySsv9000001(input)
				_, _ = impl.ConfirmSsv9000001(input)
				_, _ = impl.CancelSsv9000001(input)
			})
		})

	})
}

//func TestEul20001Impl_Eul20001_Flase(t *testing.T) {
//	input := &models.SSV9000001I{
//		AgreementId : "1",
//		PrductNm : 2,
//		AgreementType : "1",
//		Currency : "1",
//		CstmrId : "1",
//		CstmrTyp : "1",
//		AccuntNme : "1",
//		UsgCod : "1",
//		WdrwlMthd : "1",
//		AccPsw : "1",
//		TermFlag : "1",
//		BegnTxDt : "1",
//		AppntDtFlg : "1",
//		AppntDt : "1",
//		AccCnclFlg : "1",
//		AutCnl : "1",
//		SttmntFlg : "1",
//		WthdrwlMthd : "1",
//		OvrDrwFlg : "1",
//		CstmrCntctAdd : "1",
//		CstmrCntctPh : "1",
//		CstmrCntctEm : "1",
//		BnCrtAcc : "1",
//		AccOpnDt : "1",
//	}
//
//	impl := Ssv9000001Impl{
//		SSV9000001I: input,
//		O:         dao.EngineCache,
//	}
//
//	Convey("Call service", t, func() {
//		//var session *dao.Session
//		Convey("Mock data", func() {
//			db, mock, err := sqlmock.New()
//			So(err, ShouldBeNil)
//			dao.EngineCache.DB().DB = db
//			mock.ExpectQuery("t_person_argeement").WillReturnError(errors.New("xx","xx"))
//
//			mock.ExpectExec("t_person_argeement").WithArgs(input).WithArgs()
//
//			mock.ExpectQuery("t_person_argeement").WillReturnRows(sqlmock.NewRows([]string{"agreement_Id"}).AddRow("1000001"))
//
//
//			Convey("Validate session", func() {
//				_, _ = impl.TrySsv9000001(input)
//				_, _ = impl.ConfirmSsv9000001(input)
//				_, _ = impl.CancelSsv9000001(input)
//			})
//		})
//
//	})
//}
//

func TestSsv9000001Impl_json(t *testing.T) {
	addr := "localhost:3306"
	userName := "root"
	password := "123456"
	dbName := "legobank_sv"
	dsn := userName + ":" + password + "@tcp(" + addr + ")/" + dbName
	engine, err := xorm.NewEngine("mysql", dsn)
	if err != nil {
		return
	}
	maxIdleConns := beego.AppConfig.DefaultInt("mysql::maxIdleConns", 30)
	maxOpenConns := beego.AppConfig.DefaultInt("mysql::maxOpenConns", 30)
	maxLifeValue := beego.AppConfig.DefaultInt("mysql::maxLifeValue", 540)
	showSql := beego.AppConfig.DefaultBool("mysql::showSql", true)

	engine.SetMaxIdleConns(maxIdleConns)
	engine.SetMaxOpenConns(maxOpenConns)
	engine.SetConnMaxLifetime(time.Duration(maxLifeValue)*time.Second)
	engine.ShowSQL(showSql)

	EngineCache := engine

	DataMap := make(map[string]interface{})

	var TJson dao.TJson
	 _, err = EngineCache.Where("topic_id=?","2").Get(&TJson);

	_ = json.Unmarshal([]byte(TJson.Json), &DataMap)
	log.Infof("commResp: %v", DataMap)
	responseBody, _ := json.Marshal(DataMap)
	fmt.Println(string(responseBody))
}

