//Version: v0.0.1
package services

import (
	"git.forms.io/isaving/sv/ssv9000001/dao"
	"git.forms.io/isaving/sv/ssv9000001/models"
	"git.forms.io/legobank/legoapp/services"
	dtsClient "git.forms.io/universe/dts/client"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/xormplus/xorm"
	"time"
)
var Ssv9000001Compensable = dtsClient.Compensable{
	TryMethod:     "TrySsv9000001",
	ConfirmMethod: "ConfirmSsv9000001",
	CancelMethod:  "CancelSsv9000001",
}

type Ssv9000001 interface {
	TrySsv9000001(*models.SSV9000001I) (*models.SSV9000001O, error)
	ConfirmSsv9000001(*models.SSV9000001I) (*models.SSV9000001O, error)
	CancelSsv9000001(*models.SSV9000001I) (*models.SSV9000001O, error)
}

type Ssv9000001Impl struct {
	services.CommonTCCService
	SSV9000001I         *models.SSV9000001I
	SSV9000001O         *models.SSV9000001O
	O					*xorm.Engine
}
// @Desc Ssv9000001 process
// @Author
// @Date 2020-12-04
func (impl *Ssv9000001Impl) TrySsv9000001(ssv9000001I *models.SSV9000001I) (ssv9000001O *models.SSV9000001O, err error) {

	impl.SSV9000001I = ssv9000001I

	TPersonArgeement := dao.TPersonArgeement{
		AgreementId      	: ssv9000001I.AgreementId     	,
		AgreementType       : ssv9000001I.AgreementType     ,
		Currency     		: ssv9000001I.Currency     		,
		CashtranFlag        : ssv9000001I.CashtranFlag      ,
		AgreementStatus     : ssv9000001I.AgreementStatus   ,
		FreezeType          : ssv9000001I.FreezeType        ,
		TermFlag            : ssv9000001I.TermFlag          ,
		DepcreFlag          : ssv9000001I.DepcreFlag        ,
		AccFlag             : ssv9000001I.AccFlag           ,
		AccAcount           : ssv9000001I.AccAcount         ,
		BegnTxDt            : ssv9000001I.BegnTxDt          ,
		AppntDtFlg          : ssv9000001I.AppntDtFlg        ,
		AppntDt             : ssv9000001I.AppntDt           ,
		PrductId            : ssv9000001I.PrductId          ,
		PrductNm            : ssv9000001I.PrductNm          ,
		CstmrId             : ssv9000001I.CstmrId           ,
		CstmrTyp            : ssv9000001I.CstmrTyp          ,
		AccuntNme           : ssv9000001I.AccuntNme         ,
		UsgCod              : ssv9000001I.UsgCod            ,
		WdrwlMthd           : ssv9000001I.WdrwlMthd         ,
		AccPsw              : ssv9000001I.AccPsw            ,
		PswWrongTime        : ssv9000001I.PswWrongTime      ,
		TrnWdrwmThd         : ssv9000001I.TrnWdrwmThd       ,
		OpnAmt              : ssv9000001I.OpnAmt            ,
		AccCnclFlg          : ssv9000001I.AccCnclFlg        ,
		AutCnl              : ssv9000001I.AutCnl            ,
		DytoCncl            : ssv9000001I.DytoCncl          ,
		SttmntFlg           : ssv9000001I.SttmntFlg         ,
		WthdrwlMthd         : ssv9000001I.WthdrwlMthd       ,
		OvrDrwFlg           : ssv9000001I.OvrDrwFlg         ,
		CstmrCntctAdd       : ssv9000001I.CstmrCntctAdd     ,
		CstmrCntctPh        : ssv9000001I.CstmrCntctPh      ,
		CstmrCntctEm        : ssv9000001I.CstmrCntctEm      ,
		BnCrtAcc            : ssv9000001I.BnCrtAcc          ,
		BnAcc               : ssv9000001I.BnAcc             ,
		AccOpnDt            : ssv9000001I.AccOpnDt          ,
		AccOpnEm            : ssv9000001I.AccOpnEm          ,
		AccCanclDt          : ssv9000001I.AccCanclDt        ,
		AccCanclEm          : ssv9000001I.AccCanclEm        ,
		AccCanclResn        : ssv9000001I.AccCanclResn      ,
		LastUpDatetime      : ssv9000001I.LastUpDatetime    ,
		LastUpBn            : ssv9000001I.LastUpBn          ,
		LastUpEm            : ssv9000001I.LastUpEm          ,
		TccStatus			: "1",
	}
	
	if _, err = dao.EngineCache.Omit("acc_cancl_dt", "acc_cancl_em", "acc_cancl_resn").Insert(&TPersonArgeement); nil != err {
		log.Errorf("Insert TPersonArgeement, err: %s",err)
		return nil, err
	}

	TEvsarect := &dao.TEvsarect{
		AgreementId:    ssv9000001I.AgreementId,   //合约号
		AgreementType:  ssv9000001I.AgreementType,   //合约类型
		BnAcc:          ssv9000001I.BnAcc,   //开户行所
		CrcanclDate:    ssv9000001I.AccOpnDt,   //开/销户日期
		TransationDate: ssv9000001I.AccOpnDt,   //交易日期
		GlobalBizSeqNo: ssv9000001I.GlobalBizSeqNo,		//全局流水号
		SrcBizSeqNo:    ssv9000001I.SrcBizSeqNo,		//服务流水号
		RecordType:     "1",   //记录类型	1-开户;2-销户
		Currency:       ssv9000001I.Currency,   //币种
		CstmrId:        ssv9000001I.CstmrId,   //客户编号
		CstmrTyp:       ssv9000001I.CstmrTyp,   //客户类型
		AccuntNme:      ssv9000001I.AccuntNme,   //账户中文名称
		PrductId:       ssv9000001I.PrductId,   //产品号
		PrductNm:       ssv9000001I.PrductNm,   //产品名称
		TranAccType:    "1",   				//账户类型 1-客户账户;2-内部账户
		DepositNature:  "001",   			//账户性质 00-基本账户;01-一般账户
		MediumType:     ssv9000001I.MediumType,   //开户介质类型
		MediumNm:       ssv9000001I.MediumNm,   //开户介质号码
		CrtAgreemntAmt: ssv9000001I.OpnAmt,   //开户金额
		CrtcanlEm:      "",   //开/销户柜员
		CrtcanlApEm:    "",   //开/销户授权柜员
		CancelReason:   "",   //销户原因
		ContactsName:   "",   //联系人姓名
		ContactsId:     "",   //联系人id
		ContactsIdnm:    "",   //联系人姓名
		ContactsPh:      "",   //联系人电话1
		ContactsAd:      "",   //联系人地址1
		RecordStatus:   "Y",   //有效标志 Y-有效;N-无效;A-全部;-
		LastUpDatetime: time.Now().Format("2006-01-02 15:04:05"),
		LastUpBn:       "",   //
		LastUpEm:       "",   //
		TccStatus:      "1",   //
	}

	if err := dao.Insert_t_evsarect(TEvsarect); nil != err {
		return nil, err
	}
	//engine.Omit("age, gender").Insert(&user)--函数指定排除某些指定的字段
	ssv9000001O = &models.SSV9000001O{
		AgreementId: impl.SSV9000001I.AgreementId,
	}
	return ssv9000001O, nil
}

func (impl *Ssv9000001Impl) ConfirmSsv9000001(ssv9000001I *models.SSV9000001I) (ssv9000001O *models.SSV9000001O, err error) {
	log.Debug("Start confirm ssv9000001")
	sql := "update t_person_argeement set tcc_status=0 where agreement_id=?"
	if _, err := dao.EngineCache.Exec(sql, ssv9000001I.AgreementId); nil != err {
		return nil, err
	}
	sql1 := "update t_evsarect set tcc_status=0 where agreement_id=?"
	if _, err := dao.EngineCache.Exec(sql1, ssv9000001I.AgreementId); nil != err {
		return nil, err
	}
	return nil, nil
}

func (impl *Ssv9000001Impl) CancelSsv9000001(ssv9000001I *models.SSV9000001I) (ssv9000001O *models.SSV9000001O, err error) {
	log.Debug("Start cancel ssv9000001")
	sql := "delete from t_person_argeement where agreement_id=?"
	if _, err = dao.EngineCache.Exec(sql,ssv9000001I.AgreementId); nil != err {
		return nil, err
	}
	sql1 := "delete from t_evsarect where agreement_id=?"
	if _, err = dao.EngineCache.Exec(sql1,ssv9000001I.AgreementId); nil != err {
		return nil, err
	}
	return nil, nil
}
